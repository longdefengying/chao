package com.chao.mq.topicsmodel;

import com.chao.mq.RabbitMqConnectionUtil;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 超
 * Create by fengc on  2020/7/26 16:21
 */
public class TopicReveiver1 {

    private static String TOPICS_EXCHANGE = "topic_exchange";
    private static String TOPICS_ROUTING_KEY = "topics.rounting.*";
    private static String TOPICS_QUEUE = "topic_queue_01";

    public static void main(String[] args) throws IOException, TimeoutException {

        Connection connection = RabbitMqConnectionUtil.getConnection();
        Channel channel = connection.createChannel();
        //声明交换机，防止生产者没有启动
        channel.exchangeDeclare(TOPICS_EXCHANGE, BuiltinExchangeType.TOPIC);
        //声明队列
        channel.queueDeclare(TOPICS_QUEUE,false,false,false,null);
        //绑定队列
        channel.queueBind(TOPICS_QUEUE,TOPICS_EXCHANGE,TOPICS_ROUTING_KEY);
        //定义监控消费者
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String msg = new String(body,StringRpcServer.STRING_ENCODING);
                System.out.println("[consumer1] = " + msg);
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        };
        //手动确认
        channel.basicConsume(TOPICS_QUEUE,false,consumer);
    }
    
}
