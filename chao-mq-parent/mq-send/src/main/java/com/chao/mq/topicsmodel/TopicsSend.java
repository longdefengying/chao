package com.chao.mq.topicsmodel;

import com.chao.mq.RabbitMqConnectionUtil;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 超
 * Create by fengc on  2020/7/26 16:09
 */
public class TopicsSend {

    private static String TOPICS_EXCHANGE = "topic_exchange";
    private static String TOPICS_ROUTING_KEY = "topics.rounting.cat";

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = RabbitMqConnectionUtil.getConnection();
        Channel channel = connection.createChannel();
        //声明交换机
        channel.exchangeDeclare(TOPICS_EXCHANGE, BuiltinExchangeType.TOPIC);
       //发送消息
        String message = "我是一个 Topics 路由消息";
        channel.basicPublish(TOPICS_EXCHANGE,TOPICS_ROUTING_KEY,null,message.getBytes());
        System.out.println("[producer] = " + message + "----> complete");
    }

}
