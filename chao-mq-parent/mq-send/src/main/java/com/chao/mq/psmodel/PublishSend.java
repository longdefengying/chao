package com.chao.mq.psmodel;

import com.chao.mq.RabbitMqConnectionUtil;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 超
 * Create by fengc on  2020/7/26 14:09
 * 交换机主要用到广播
 */
public class PublishSend {

    private static String EXCHANGE_NAME = "fanout_exchange";

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = RabbitMqConnectionUtil.getConnection();
        //获取渠道
        Channel channel = connection.createChannel();
        //声明Exchange，执行是fanout类型
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT.getType());
        String message = "register success!";
        //发布消息到Exchange
        channel.basicPublish(EXCHANGE_NAME,"",null,message.getBytes());
        System.out.println("[producer] send :" + message );
        channel.close();
        connection.close();
    }
}
