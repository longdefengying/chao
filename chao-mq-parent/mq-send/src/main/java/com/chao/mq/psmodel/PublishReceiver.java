package com.chao.mq.psmodel;

import com.chao.mq.RabbitMqConnectionUtil;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 超
 * Create by fengc on  2020/7/26 14:25
 */
public class PublishReceiver {

    private static String EXCHANGE_NAME = "fanout_exchange";

    private static String QUEUE_NAME = "fanout_queue";

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = RabbitMqConnectionUtil.getConnection();
        Channel channel = connection.createChannel();
        //声明队列
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        //绑定队列到交换机
        channel.queueBind(QUEUE_NAME,EXCHANGE_NAME,"");
        //定义队列的消费者
        Consumer consumer = new DefaultConsumer(channel) {
            //监听队列，是否有消息
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String msg = new String(body, StringRpcServer.STRING_ENCODING);
                System.out.println("[receiver]  msg = " + msg);
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        };
        //手动确认消息
        channel.basicConsume(QUEUE_NAME,false,consumer);
    }
}
