package com.chao.mq;

import com.rabbitmq.client.*;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @Author: fengchao
 * @Date: 2020/7/24 17:06
 * @Version 1.0
 * @decription todo 建立与rabbitMQ的连接
 */
public class RabbitMqConnectionUtil {

    private static String MQ_HOST = "192.168.2.104";
    private static int MQ_PORT = 5672;
    private static String VIRTUAL_HOST = "/mall";
    private static String USER_NAME = "admin";
    private static String PASSWORD = "admin";

    public static Connection getConnection() throws IOException, TimeoutException {
        //定义连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(MQ_HOST);
        factory.setPort(MQ_PORT);
        //设置用户、密码、vhost
        factory.setVirtualHost(VIRTUAL_HOST);
        factory.setUsername(USER_NAME);
        factory.setPassword(PASSWORD);
        //通过工厂获取连接
        Connection connection = factory.newConnection();
        System.out.println("获取到的MQ连接：" + connection.toString());
        return connection;
    }
}
