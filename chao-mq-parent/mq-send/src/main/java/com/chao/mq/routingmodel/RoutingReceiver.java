package com.chao.mq.routingmodel;

import com.chao.mq.RabbitMqConnectionUtil;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 超
 * Create by fengc on  2020/7/26 15:07
 */
public class RoutingReceiver {

    private static String EXCHANGE_NAME = "direct_routing_exchange";
    private static String ROUTING_KEY = "direct_routing_key";
    private static String ROUTING_QUEUE = "direct_routing_queue";

    public static void main(String[] args) throws IOException, TimeoutException {
       Connection connection = RabbitMqConnectionUtil.getConnection();
       Channel channel = connection.createChannel();
        //声明 exchange,是direct模式
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
       //声明队列 durable = true 队列持久化
       channel.queueDeclare(ROUTING_QUEUE,true,false,false,null);
       //绑定队列的交换机
       channel.queueBind(ROUTING_QUEUE,EXCHANGE_NAME,ROUTING_KEY);
       //定义队列的消费者
       Consumer consumer = new DefaultConsumer(channel) {
           //重新复写监听队列的方法
           @Override
           public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
              //转换body
              String msg = new String(body,StringRpcServer.STRING_ENCODING);
              System.out.println("[consumer] = " + msg);
           }
       };
       //监听队列，自动ACK
       channel.basicConsume(ROUTING_QUEUE,true,consumer);
    }
}
