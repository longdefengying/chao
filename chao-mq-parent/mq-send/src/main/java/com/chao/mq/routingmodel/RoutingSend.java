package com.chao.mq.routingmodel;

import com.chao.mq.RabbitMqConnectionUtil;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author 超
 * Create by fengc on  2020/7/26 14:51
 */
public class RoutingSend {

    private static String EXCHANGE_NAME = "direct_routing_exchange";
    private static String ROUTING_KEY = "direct_routing_key";

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = RabbitMqConnectionUtil.getConnection();
        Channel channel = connection.createChannel();
        //声明 exchange,是direct模式, durable = true 交换机持久化
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT,true);
        //发送的消息内容
        String message = "这是注册的消息呀。";
        /**
         * MessageProperties.PERSISTENT_TEXT_PLAIN 发送的消息持久化
         */
        channel.basicPublish(EXCHANGE_NAME,ROUTING_KEY, MessageProperties.PERSISTENT_TEXT_PLAIN,message.getBytes());
        System.out.println("[send] = " + message);
        channel.close();
        connection.close();
    }
}
