package com.chao.mq.workmodel.sender;

import com.chao.mq.RabbitMqConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @Author: fengchao
 * @Date: 2020/7/24 23:43
 * @Version 1.0
 * @decription todo
 */
public class MessageSend {

    private static String QUEUE_NAME = "mall_queue_work";

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        //1、获取MQ的连接
        Connection connection = RabbitMqConnectionUtil.getConnection();
        //2、从连接中创建渠道
        Channel channel = connection.createChannel();
        /**
         * 3、声明队列
         * 参数明细：
         * Declare a queue
         * @see com.rabbitmq.client.AMQP.Queue.Declare
         * @see com.rabbitmq.client.AMQP.Queue.DeclareOk
         * @param queue the name of the queue 队列名称
         * @param durable true if we are declaring a durable queue (the queue will survive a server restart) 是否持久化、如果持久化，那么重启队列后还存在
         * @param exclusive true if we are declaring an exclusive queue (restricted to this connection) 是否独占连接 队列只允许在该连接中访问，如果connection连接关闭队列则自动删除,如果将此参数设置true可用于临时队列的创建
         * @param autoDelete true if we are declaring an autodelete queue (server will delete it when no longer in use) 自动删除，队列不再使用时是否自动删除此队列，如果将此参数和exclusive参数设置为true就可以实现临时队列（队列不用了就自动删除）
         * @param arguments other properties (construction arguments) for the queue 参数，可以设置一个队列的扩展参数，比如：可设置存活时间
         * @return a declaration-confirm method to indicate the queue was successfully declared
         * @throws IOException if an error is encountered
         */
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        String message = "Hello world,moto";
        //循环发送消息
        for (int i = 0 ; i < 100 ; i ++) {
            String actMessage = message + "-->" + i;
            channel.basicPublish("",QUEUE_NAME,null,actMessage.getBytes());
            System.out.println("send = "  + actMessage);
            //TimeUnit.SECONDS.sleep(1);
        }
        //关闭通道和连接
        channel.close();
        connection.close();
    }

}
