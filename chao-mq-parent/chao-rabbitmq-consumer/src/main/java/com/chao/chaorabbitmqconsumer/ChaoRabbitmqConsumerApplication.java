package com.chao.chaorabbitmqconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChaoRabbitmqConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChaoRabbitmqConsumerApplication.class, args);
    }
}
