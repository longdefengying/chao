package com.spring.pure;

import org.apache.commons.dbcp2.BasicDataSourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import javax.xml.crypto.Data;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author 超
 * Create by fengc on  2022/3/19 20:01
 */
@Configuration
//事务管理注解
@EnableTransactionManagement
public class PureDatasourceApp {

    @Autowired
    private DataSource dataSource;

    public static void main(String[] args) throws Exception {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext*.xml");
        showBeans(applicationContext);
        datasourceDemo(applicationContext);
    }

    /*@Bean
    public DataSource dataSource() throws Exception {
        Properties properties = new Properties();
        properties.setProperty("driverName","org.h2.Driver");
        properties.setProperty("url","jdbc:h2:mem:testdb");
        properties.setProperty("username","admin");
        return BasicDataSourceFactory.createDataSource(properties);
    }*/

    @Bean
    public PlatformTransactionManager transactionManager() throws Exception {
        return new DataSourceTransactionManager(dataSource);
    }

    public static void showBeans(ApplicationContext applicationContext) {
        System.out.println("applicationContext.getApplicationName():" + applicationContext.getApplicationName());
    }

    public static void datasourceDemo(ApplicationContext applicationContext) throws SQLException {
        PureDatasourceApp pureDatasourceApp = applicationContext.getBean("pureDatasourceApp",PureDatasourceApp.class);
        pureDatasourceApp.showDataSource();
    }

    public void showDataSource() throws SQLException {
        System.out.println("dataSource.toString(): " + dataSource.toString());
        Connection connection = dataSource.getConnection();
        System.out.println("connection:" + connection);
        connection.close();
    }

}
