package com.example.springboottransaction.controller;

import com.example.springboottransaction.model.SysUser;
import com.example.springboottransaction.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 超
 * Create by fengc on  2022/3/7 23:56
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class SysUserController {

    @Autowired
    private SysUserService userService;

    @GetMapping("/getUsersByName")
    public List<SysUser> getUsersByName(@RequestParam("name") String name) {
        log.info("-----------name={}",name);
        return userService.findByUserName(name);
    }

}
