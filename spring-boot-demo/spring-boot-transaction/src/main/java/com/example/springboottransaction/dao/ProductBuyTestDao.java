package com.example.springboottransaction.dao;

import com.example.springboottransaction.model.ProductBuyTest;
import org.springframework.data.repository.CrudRepository;

/**
 * @author 超
 * Create by fengc on  2022/3/8 20:04
 */
public interface ProductBuyTestDao extends CrudRepository<ProductBuyTest,Long> {

}
