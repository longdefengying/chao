package com.example.springboottransaction.dao;

import com.example.springboottransaction.model.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author 超
 * Create by fengc on  2022/3/7 23:51
 */
public interface SysUserDao extends JpaRepository<SysUser,Long> {

    List<SysUser> findSysUserByUsernameLike(String userName);
}
