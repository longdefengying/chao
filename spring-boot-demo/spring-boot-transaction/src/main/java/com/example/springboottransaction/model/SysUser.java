package com.example.springboottransaction.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 *
 * @author chao
 * @date 2021-11-16 16:08:24
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Entity
public class SysUser implements Serializable {
    private static final long serialVersionUID=1L;

        @Id
        @GeneratedValue(strategy= GenerationType.AUTO)
        private Long id;
        private Date createTime;
        private Date updateTime;
        private String username;
        private String password;
        private String nickname;
        private String headImgUrl;
        private String mobile;
        private Byte sex;
        private Byte enabled;
        private String type;
        private String company;
        private String openId;
        private Byte isDel;
    }
