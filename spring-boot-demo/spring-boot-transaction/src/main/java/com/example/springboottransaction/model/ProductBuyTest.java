package com.example.springboottransaction.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author 超
 * Create by fengc on  2022/3/8 19:52
 */
@Data
@Entity
@Table(name = "t_product_buy_test")
public class ProductBuyTest implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String buyName;

    private String productName;

    @Column(name = "create_time")
    private Date createTime;
}
