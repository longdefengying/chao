package com.example.springboottransaction.service;

import com.example.springboottransaction.dao.SysUserDao;
import com.example.springboottransaction.model.SysUser;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 超
 * Create by fengc on  2022/3/7 23:53
 */
@Service
public class SysUserService {

    @Resource
    private SysUserDao sysUserDao;

    public List<SysUser> findByUserName(String userName) {

        return sysUserDao.findSysUserByUsernameLike(userName + "%");
    }
}
