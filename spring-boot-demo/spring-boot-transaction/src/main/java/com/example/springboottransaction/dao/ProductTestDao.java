package com.example.springboottransaction.dao;

import com.example.springboottransaction.model.ProductTest;
import org.springframework.data.repository.CrudRepository;

/**
 * @author 超
 * Create by fengc on  2022/3/8 20:04
 */
public interface ProductTestDao extends CrudRepository<ProductTest,Long> {


}
