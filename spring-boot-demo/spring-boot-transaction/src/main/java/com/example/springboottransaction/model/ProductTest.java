package com.example.springboottransaction.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author 超
 * Create by fengc on  2022/3/8 19:46
 */
@Data
@Entity
@Table(name = "t_product_test")
public class ProductTest implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String productName;

    private Integer productCount;

}
