package com.example.springboottransaction.service;

import com.example.springboottransaction.dao.ProductBuyTestDao;
import com.example.springboottransaction.dao.ProductTestDao;
import com.example.springboottransaction.model.ProductBuyTest;
import com.example.springboottransaction.model.ProductTest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author 超
 * Create by fengc on  2022/3/8 19:57
 */
@Slf4j
@Service
public class ProductTestService {

    private Lock lock = new ReentrantLock(true);
    @Autowired
    private ProductTestDao productTestDao;
    @Autowired
    private ProductBuyTestDao productBuyTestDao;

    @Transactional
    public void sellProduct() {
        try {
            lock.lock();
            log.info("{},抢到了锁啦，进入方法.......",Thread.currentThread().getName());
            ProductTest productTest = productTestDao.findById(1l).orElseGet(ProductTest::new);
            Integer productCount = productTest.getProductCount();
            log.info("{},当前库存={}",Thread.currentThread().getName(),productCount);
            if (Objects.nonNull(productCount) && productCount > 0) {
                productTest.setProductCount(productCount -1);
                productTestDao.save(productTest);
                ProductBuyTest productBuyTest = new ProductBuyTest();
                productBuyTest.setBuyName(Thread.currentThread().getName());
                productBuyTest.setProductName(productTest.getProductName());
                productBuyTest.setCreateTime(new Date());
                productBuyTestDao.save(productBuyTest);
                log.info("{}，抢购一件商品完毕.......",Thread.currentThread().getName());
                Thread.sleep(100);
            } else {
                log.info("{}....抢购不到商品......",Thread.currentThread().getName());
            }
        } catch (Exception e) {
            log.error("error !!!!,",e);
        }finally {
            log.info("{},开始释放锁..........",Thread.currentThread().getName());
            lock.unlock();
        }
    }
}
