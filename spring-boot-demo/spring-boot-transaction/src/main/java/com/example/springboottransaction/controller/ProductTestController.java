package com.example.springboottransaction.controller;

import com.example.springboottransaction.service.ProductTestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CountDownLatch;

/**
 * @author 超
 * Create by fengc on  2022/3/8 20:27
 */
@RestController
@RequestMapping("/product")
@Slf4j
public class ProductTestController {

    @Autowired
    private ProductTestService productTestService;

    @GetMapping("/seller")
    public String seller() {
        CountDownLatch countDownLatch = new CountDownLatch(3);
        for (int i = 0 ; i < 2 ; i++) {
            new Thread(()-> {
                try {
                    countDownLatch.await();
                    productTestService.sellProduct();
                } catch (InterruptedException ie) {
                    log.error("错误信息-->,",ie);
                }
            }).start();
            countDownLatch.countDown();
        }
        System.out.println("开始释放锁....,准备开抢...................................................");
        countDownLatch.countDown();
        return "商品抢购完毕!!!!!!!!!!!!";
    }


}
