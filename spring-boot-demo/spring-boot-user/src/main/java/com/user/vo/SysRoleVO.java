package com.user.vo;

import com.common.core.model.SuperEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 
 *
 * @author chao
 * @date 2021-11-16 16:08:24
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class SysRoleVO extends SuperEntity {
    private static final long serialVersionUID=1L;

            private String Code;
            private String Name;
            private Date CreateTime;
            private Date UpdateTime;
            private String TenantId;
    }
