package com.user.vo;
import com.common.core.model.SuperEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 
 *
 * @author chao
 * @date 2021-11-16 16:08:24
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class SysMenuVO extends SuperEntity {
    private static final long serialVersionUID=1L;

            private Integer ParentId;
            private String Name;
            private String Url;
            private String Path;
            private String PathMethod;
            private String Css;
            private Integer Sort;
            private Date CreateTime;
            private Date UpdateTime;
            private Byte Type;
            private Byte Hidden;
            private String TenantId;
    }
