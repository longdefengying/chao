package com.user.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.common.core.model.Result;
import com.user.model.SysMenu;
import com.user.service.SysMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 
 *
 * @author chao
 * @date 2021-11-16 16:08:24
 */
@Slf4j
@RestController
@RequestMapping("/sysmenu")
//@Api(tags = "")
public class SysMenuController {

    @Autowired
    private SysMenuService sysMenuService;

    /**
     * 列表
     */
    //@ApiOperation(value = "查询列表")
    /*@ApiImplicitParams({
            //@ApiImplicitParam(name = "page", value = "分页起始位置", required = true, dataType = "Integer"),
            //@ApiImplicitParam(name = "limit", value = "分页结束位置", required = true, dataType = "Integer")
    })*/
    @GetMapping
    public IPage<SysMenu> list(@RequestParam Map<String, Object> params) {
        return sysMenuService.findList(params);
    }

    /**
     * 查询
     */
    //@ApiOperation(value = "查询")
    @GetMapping("/{Id}")
    public Result findUserById(@PathVariable Long Id) {
        SysMenu model = sysMenuService.getById(Id);
        return Result.success(model, "查询成功");
    }

    /**
     * 新增or更新
     */
    //@ApiOperation(value = "保存")
    @PostMapping
    public Result save(@RequestBody SysMenu sysMenu) {
        sysMenuService.saveOrUpdate(sysMenu);
        return Result.success("保存成功");
    }

    /**
     * 删除
     */
    //@ApiOperation(value = "删除")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Long Id) {
        sysMenuService.removeById(Id);
        return Result.success("删除成功");
    }
}
