package com.user.controller;

import com.common.core.model.Result;
import com.user.model.SysRoleMenu;
import com.user.service.SysRoleMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 
 *
 * @author chao
 * @date 2021-11-16 16:08:24
 */
@Slf4j
@RestController
@RequestMapping("/sysrolemenu")
//@Api(tags = "")
public class SysRoleMenuController {
    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    /**
     * 列表
     */
 /*   //@ApiOperation(value = "查询列表")
    //@ApiImplicitParams({
            //@ApiImplicitParam(name = "page", value = "分页起始位置", required = true, dataType = "Integer"),
            //@ApiImplicitParam(name = "limit", value = "分页结束位置", required = true, dataType = "Integer")
    })
    @GetMapping
    public PageResult list(@RequestParam Map<String, Object> params) {
        return sysRoleMenuService.findList(params);
    }*/

    /**
     * 查询
     */
    //@ApiOperation(value = "查询")
    @GetMapping("/{RoleId}")
    public Result findUserById(@PathVariable Long RoleId) {
        SysRoleMenu model = sysRoleMenuService.getById(RoleId);
        return Result.success(model, "查询成功");
    }

    /**
     * 新增or更新
     */
    //@ApiOperation(value = "保存")
    @PostMapping
    public Result save(@RequestBody SysRoleMenu sysRoleMenu) {
        sysRoleMenuService.saveOrUpdate(sysRoleMenu);
        return Result.success("保存成功");
    }

    /**
     * 删除
     */
    //@ApiOperation(value = "删除")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Long RoleId) {
        sysRoleMenuService.removeById(RoleId);
        return Result.success("删除成功");
    }
}
