package com.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 超
 * Create by fengc on  2022/3/4 09:50
 */
@SpringBootApplication
public class SpringbootUserApplication {

    public static void main(String[] args) {
        //BasicConfigurator.configure();
        SpringApplication.run(SpringbootUserApplication.class, args);
        System.out.println("项目启动成功....................................");
    }
}
