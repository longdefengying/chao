package com.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.user.model.SysMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chao
 * @date 2021-11-16 16:08:24
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
