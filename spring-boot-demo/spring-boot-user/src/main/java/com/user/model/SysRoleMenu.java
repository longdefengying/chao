package com.user.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.common.core.model.SuperEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 *
 * @author chao
 * @date 2021-11-16 16:08:24
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@TableName("sys_role_menu")
public class SysRoleMenu extends SuperEntity {
    private static final long serialVersionUID=1L;

        private Integer MenuId;
    }
