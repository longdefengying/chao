package com.user.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.common.core.model.SuperEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 
 *
 * @author chao
 * @date 2021-11-16 16:08:24
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@TableName("sys_user")
public class SysUser extends SuperEntity {
    private static final long serialVersionUID=1L;

        private String Username;
        private String Password;
        private String Nickname;
        private String HeadImgUrl;
        private String Mobile;
        private Byte Sex;
        private Byte Enabled;
        private String Type;
        private Date CreateTime;
        private Date UpdateTime;
        private String Company;
        private String OpenId;
        private Byte IsDel;
    }
