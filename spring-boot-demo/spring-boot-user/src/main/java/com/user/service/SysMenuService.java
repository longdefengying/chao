package com.user.service;

import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.user.mapper.SysMenuMapper;
import com.user.model.SysMenu;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 
 *
 * @author chao
 * @date 2021-11-16 16:08:24
 */
@Slf4j
@Service
public class SysMenuService extends ServiceImpl<SysMenuMapper, SysMenu> {

    public IPage<SysMenu> findList(Map<String, Object> params) {
        Page<SysMenu> page =new Page<>(MapUtil.getInt(params,"pageNum"),MapUtil.getInt(params,"pageSize"));
        IPage<SysMenu> pageResult = this.page(page);
        return pageResult;
    }

}

