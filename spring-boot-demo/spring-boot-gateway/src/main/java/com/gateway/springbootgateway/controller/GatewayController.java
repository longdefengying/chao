package com.gateway.springbootgateway.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 超
 * Create by fengc on  2021/12/16 23:37
 */
@RestController
@RequestMapping("/gateway")
@Slf4j
public class GatewayController {

    @GetMapping("/hello")
    public String hello () {
        log.info("hello----------------------------------------");
        return "Hello , Java!";
    }

}
