package com.gateway.springbootgateway;

import com.spire.pdf.PdfDocument;
import com.spire.pdf.PdfPageBase;
import com.spire.pdf.annotations.PdfRubberStampAnnotation;
import com.spire.pdf.annotations.appearance.PdfAppearance;
import com.spire.pdf.graphics.PdfImage;
import com.spire.pdf.graphics.PdfTemplate;

import java.awt.geom.Rectangle2D;

/**
 * @author 超
 * Create by fengc on  2022/7/19 18:51
 */
public class PdfMain {

    public static void main(String[] args) {
       // pdfAppearance();
        pdfImage();
    }

    public static void pdfImage() {
        //创建文档
        PdfDocument pdf = new PdfDocument();
        //加载PDF文档
        pdf.loadFromFile("F:\\excel\\2020年《Java面试题手册》.pdf");
        //获取最后一页
        PdfPageBase page = pdf.getPages().get(pdf.getPages().getCount() -1);
        //加载图片，并获取图片高宽
        PdfImage image = PdfImage.fromFile("F:\\excel\\image002.png");
        int width = image.getWidth()/2;
        int height = image.getHeight()/2;
        page.getCanvas().drawImage(image,(float) (page.getActualSize().getWidth() - width - 30),
                (float) (page.getActualSize().getHeight() - height - 60),width, height);

        PdfImage image1 = PdfImage.fromFile("F:\\excel\\chihe.jpeg");
        int width1 = image1.getWidth()/2;
        int height1 = image1.getHeight()/2;

        page.getCanvas().drawImage(image1,(float) (page.getActualSize().getWidth() - width - 190),
                (float) (page.getActualSize().getHeight() - height - 160),width1, height1);

        //保存文档
        pdf.saveToFile("F:\\excel\\image5.pdf");
        pdf.dispose();
        pdf.close();

    }

    public static void pdfAppearance() {
        //创建文档
        PdfDocument pdf = new PdfDocument();
        //加载PDF文档
        pdf.loadFromFile("F:\\excel\\测试的.pdf");

        //获取第一页
        PdfPageBase page = pdf.getPages().get(pdf.getPages().getCount() -1);

        //加载图片，并获取图片高宽
        PdfImage image = PdfImage.fromFile("F:\\excel\\image002.png");
        //获取图片高宽
        int width = image.getWidth() / 2;
        int height = image.getHeight() / 2;

        //创建PdfTemplate对象，大小跟图片一致
        PdfTemplate template = new PdfTemplate(width, height);

        //在模板上绘制图片
        template.getGraphics().drawImage(image, 0, 0, width, height);

        //创建PdfRubebrStampAnnotation对象，指定大小和位置
        Rectangle2D rect = new Rectangle2D.Float((float) (page.getActualSize().getWidth() - width - 30),
                (float) (page.getActualSize().getHeight() - height - 60), width, height);
        PdfRubberStampAnnotation stamp = new PdfRubberStampAnnotation(rect);

        //创建PdfAppearance对象
        PdfAppearance pdfAppearance = new PdfAppearance(stamp);

        //将模板应用为PdfAppearance的一般状态
        pdfAppearance.setNormal(template);

        //将PdfAppearance 应用为图章的样式
        stamp.setAppearance(pdfAppearance);

        //添加图章到PDF
        page.getAnnotationsWidget().add(stamp);

        //保存文档
        pdf.saveToFile("F:\\excel\\2.pdf");
        pdf.dispose();
    }

}
