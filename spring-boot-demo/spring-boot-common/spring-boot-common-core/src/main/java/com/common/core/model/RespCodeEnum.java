package com.common.core.model;

/**
 * @author 超
 * Create by fengc on  2021/6/14 23:42
 */
public enum RespCodeEnum {
    SUCCESS(0),ERROR(1);
    private Integer code;

    RespCodeEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

}
