package com.datasource.springdatasourcedemo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

@SpringBootApplication
@Slf4j
public class SpringDatasourceDemoApplication implements CommandLineRunner {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static void main(String[] args) {

        SpringApplication.run(SpringDatasourceDemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        showConnection();
        showDate();
    }

    private void showConnection() throws Exception{
        log.info("dataSource.toString()={}....................",dataSource.toString());
        Connection con = dataSource.getConnection();
        Statement stmt = con.createStatement();
        ResultSet rs1= stmt.executeQuery("select BAR from foo where id=1");
        ResultSet rs2= stmt.executeQuery("select BAR from foo where id=1");
        if(rs1.next())System.out.println(rs1.getString(1));
        if(rs2.next())System.out.println(rs2.getString(1));
        log.info("con.toString()={}............................",con.toString());
        con.close();
    }

    private void showDate() {
        jdbcTemplate.queryForList("select * from foo").stream().forEach(row -> log.info("数据展示：{}",row));
    }
}
