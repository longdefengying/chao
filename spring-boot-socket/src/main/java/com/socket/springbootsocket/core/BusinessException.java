package com.socket.springbootsocket.core;

/**
 * @author 超
 * Create by fengc on  7/10/2023 23:04
 * 业务异常
 */
public class BusinessException  extends RuntimeException{
    private Integer errorCode;


    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public BusinessException(ErrorInfoEntity errorInfoEntity) {
        super(errorInfoEntity.getErrorMsg());
        this.errorCode = errorInfoEntity.getErrorCode();
    }

    public BusinessException(ErrorInfoEntity errorInfoEntity,Object... info) {
        super(String.format(errorInfoEntity.getErrorMsg(),info));
        this.errorCode = errorInfoEntity.getErrorCode();
    }

    public static void main(String[] args) {
        System.out.println(String.format("你是一个%s%s","这些事","这是吗"));
    }
}
