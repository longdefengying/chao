package com.socket.springbootsocket.core;

/**
 * @author 超
 * Create by fengc on  7/10/2023 22:43
 */
public enum OperationCode {
    /**
     * 404 找不到出错
     */
    ERROR_404(404),

    /**
     * 会话超时，请重新登陆
     */
    ESYS_0001(50014),

    /**
     * 用户鉴权失败
     */
    ESYS_9998(50008),

    /**
     * 系统内部异常
     */
    ESYS_9999(40001),

    /**
     * 用户重复登录
     */
    EUOP_0001(50012),

    /**
     * 参数错误
     */
    ESYS_10000(40002),

    /**
     * 操作成功!
     */
    IPER_0001(20000);
    ;

    private Integer code;

    OperationCode(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
