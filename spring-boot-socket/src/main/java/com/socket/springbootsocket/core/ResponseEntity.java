package com.socket.springbootsocket.core;

/**
 * @author 超
 * Create by fengc on  7/10/2023 22:49
 * 接口规范返回
 */
public class ResponseEntity<T> {

    private Integer code;

    private String message;

    private T date;

    public ResponseEntity<T> OK(T data) {
        this.setCode(OperationCode.IPER_0001.getCode());
        this.setDate(data);
        return this;
    }

    public ResponseEntity<T> OK () {
        return OK(null);
    }

    public ResponseEntity<T> OK (T date,String message) {
        this.OK(date).setMessage(message);
        return this;
    }

    public ResponseEntity<T> ERROR(BusinessException exception) {
        this.setMessage(exception.getMessage());
        this.setCode(exception.getErrorCode());
        return this;
    }

    public ResponseEntity<T> ERROR(T data,BusinessException exception) {
        this.setDate(data);
        this.setMessage(exception.getMessage());
        this.setCode(exception.getErrorCode());
        return this;
    }


    public Integer getCode() {
        return code;
    }

    @Deprecated
    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    @Deprecated
    public void setMessage(String message) {
        this.message = message;
    }

    public T getDate() {
        return date;
    }

    @Deprecated
    public void setDate(T date) {
        this.date = date;
    }

    public static <T> ResponseEntity<T> success() {
        return new ResponseEntity<T>().OK();
    }

    public static <T> ResponseEntity<T> success(T data) {
        return new ResponseEntity<T>().OK(data);
    }

    public static <T> ResponseEntity<T> success(T data, String message) {
        return new ResponseEntity<T>().OK(data,message);
    }

    public ResponseEntity<T> fail() {

        return null;
    }
}
