package com.socket.springbootsocket.core;

/**
 * @author 超
 * Create by fengc on  7/11/2023 10:44
 */
public interface ErrorInfoEntity {

    /**
     * 获取错误信息
     * @return
     */
    String getErrorMsg();

    /**
     * 获取错误码
     * @return
     */
    Integer getErrorCode();
}
