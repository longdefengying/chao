package com.socket.springbootsocket.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author 超
 * Create by fengc on  2023/1/6 10:56
 */
@Configuration
@Slf4j
public class ThreadPoolConfiguration {

    @Bean(name = "clientTaskPool")
    public ThreadPoolTaskExecutor clientTaskPool() {
        log.info("初始化..线程池...clientTaskPool......");
        ThreadPoolTaskExecutor taskExecutor =new ThreadPoolTaskExecutor();
        //核心线程数：线程池创建时候初始化的线程数
        taskExecutor.setCorePoolSize(5);
        //允许线程空闲时间：当超过核心线程数之外的线程在空闲时间超过60秒后，就会销毁
        taskExecutor.setKeepAliveSeconds(60);
        //最大线程数：只有在缓冲队列满了、核心线程池也都用完了之后才会申请超过核心线程数的线程
        taskExecutor.setMaxPoolSize(30);
        //缓冲队列：用来缓冲执行任务的队列
        taskExecutor.setQueueCapacity(100);
        //线程池名字的前缀
        taskExecutor.setThreadNamePrefix("clientTaskPool");
        //缓冲队列满了后的拒绝策略：由调用线程处理（一般是主线程）
        taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        taskExecutor.initialize();
        return taskExecutor;
    }

    @Bean(name = "clientMessageTaskPool")
    public ThreadPoolTaskExecutor clientMessageTaskPool() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(5);
        taskExecutor.setMaxPoolSize(30);
        taskExecutor.setQueueCapacity(500);
        taskExecutor.setKeepAliveSeconds(60);
        taskExecutor.setThreadNamePrefix("clientMessageTaskPool");
        taskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return taskExecutor;
    }
}
