package com.socket.springbootsocket.config;

import com.alibaba.fastjson.JSONObject;
import com.socket.springbootsocket.socket.server.SocketServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 超
 * Create by fengc on  6/29/2023 19:03
 */
@Configuration
@Slf4j
public class SocketServerConfig {

    @Bean
    public SocketServer socketServer() {
        log.info("开始初始化...SocketServer...");
        SocketServer socketServer = new SocketServer(60000);
        socketServer.setLonginHandler(userId -> {
            log.info("处理socket用户身份验证：userId={}",userId);
            return userId.contains("login_");
        });
        socketServer.setMessageHandler(((connection, messageDto) ->
                log.info("处理socket消息：connection={},messageDto={}",JSONObject.toJSONString(connection),JSONObject.toJSONString(messageDto))));
        socketServer.start();
        return socketServer;
    }

}
