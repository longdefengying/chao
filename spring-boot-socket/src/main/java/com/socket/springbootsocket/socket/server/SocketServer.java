package com.socket.springbootsocket.socket.server;

import com.socket.springbootsocket.socket.constants.SocketConstant;
import com.socket.springbootsocket.socket.handler.LonginHandler;
import com.socket.springbootsocket.socket.handler.MessageHandler;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author 超
 * Create by fengc on  2023/1/6 16:00
 */
@Slf4j
@Data
public class SocketServer {

    private ServerSocket serverSocket;

    /**
     * 监听主线程
     */
    private ListeningThread listeningThread;

    /**
     * 消息处理器
     */
    private MessageHandler messageHandler;

    /**
     * 登录处理器
     */
    private LonginHandler longinHandler;

    /**
     * 用户扫已有的socket 处理线程
     * 1、没有线程的不引用
     * 2、关注是否有心跳
     * 3、关注是否超过登录时间
     */
    private ScheduledExecutorService scheduledSocketMonitorExecutor = Executors
            .newSingleThreadScheduledExecutor(r -> new Thread(r,"socket_monitor_" + r.hashCode()));

    /**
     * 存储当前由用户活跃的socket线程
     */
    private ConcurrentHashMap<String,Connection> existSocketMap = new ConcurrentHashMap<>();

    /**
     * 存储只要有socket处理的线程
     */
    private List<ConnectionThread> existConnectionThreadList = Collections.synchronizedList(new ArrayList<>());

    /**
     * 中间list ,用户遍历的时候删除
     */
    private List<ConnectionThread> noConnectionThreadList = Collections.synchronizedList(new ArrayList<>());

    /**
     * 本地启动线程
     * @param port
     */
    public SocketServer(int port) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException ioe) {
            log.error("本地启动socket服务失败，错误信息：",ioe);
        }
    }

    /**
     * 开启一个线程来开启本地socket服务，开启一个monitor线程
     */
    public void start() {
        listeningThread = new ListeningThread(this);
        listeningThread.start();
        //每隔1秒扫描一次ThreadList
        scheduledSocketMonitorExecutor.scheduleWithFixedDelay(() -> {
            existConnectionThreadList.forEach(connectionThread -> {
                if (!connectionThread.isRunning()) {
                    noConnectionThreadList.add(connectionThread);
                } else {
                    Date lastOnTime = connectionThread.getConnection().getLastOnTime();
                    long heartDuration = Calendar.getInstance().getTimeInMillis() - lastOnTime.getTime();
                    if (heartDuration > SocketConstant.HEART_RATE) {

                    }
                }
            });
        },0,1, TimeUnit.SECONDS);
    }

    /**
     * 关闭本地服务
     */
    public void close() {

        try {
            //先关闭monitor线程，防止遍历list的时候
            scheduledSocketMonitorExecutor.shutdownNow();
            if (serverSocket != null && !serverSocket.isClosed()) {
                listeningThread.stopRunning();
                listeningThread.suspend();
                listeningThread.stop();
                serverSocket.close();
            }
        } catch (IOException ioe) {
            log.error("Socket Server close fail exception : ",ioe);
        }
    }
}
