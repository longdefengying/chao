package com.socket.springbootsocket.socket.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author 超
 * Create by fengc on  3/15/2023 17:02
 */
@Data
@Accessors(chain = true)
public class MessageDto implements Serializable {

    /**
     * 状态码，2000成功
     * 999 失败
     */
    private Integer statusCode;

    private String errorMessage;
    /**
     * 功能码：0=心跳，1=登录，2=退出，3=发送消息
     */
    private Integer functionCode;

    /**
     * 用户
     */
    private String userId;

    /**
     * 消息体
     */
    private String message;

}
