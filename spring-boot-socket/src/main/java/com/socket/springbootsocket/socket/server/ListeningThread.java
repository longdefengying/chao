package com.socket.springbootsocket.socket.server;

import com.alibaba.fastjson.JSONObject;
import com.socket.springbootsocket.socket.constants.SocketConstant;
import com.socket.springbootsocket.socket.dto.MessageDto;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author 超
 * Create by fengc on  2023/1/6 16:01
 */
@Slf4j
public class ListeningThread extends Thread{

    private SocketServer socketServer;

    private ServerSocket serverSocket;

    private boolean isRunning;

    public ListeningThread(SocketServer socketServer) {
        this.socketServer = socketServer;
        this.serverSocket = socketServer.getServerSocket();
        isRunning = true;
        log.info("socket服务端开始监听.........");
    }

    @Override
    public void run() {
        while (isRunning) {
            if (serverSocket.isClosed()) {
                isRunning = false;
                break;
            }
            try {
                Socket socket = serverSocket.accept();
                socket = serverSocket.accept();
                if (socketServer.getExistConnectionThreadList().size() > SocketConstant.MAX_SOCKET_THREAD_NUM) {
                    //超过线程数
                    PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()),true);
                    writer.println(JSONObject.toJSONString(
                            new MessageDto().setStatusCode(SocketConstant.STATUS_CODE_FAIL)
                                    .setErrorMessage("已经超过最大连接数，请稍后再试!")
                    ));
                    socket.close();
                }
                //设置超时时间为5s （有心跳机制不需要设置）
                //socket.setSoTimeout(5 * 1000);
                ConnectionThread connectionThread  = new ConnectionThread(socket,socketServer);
                socketServer.getExistConnectionThreadList().add(connectionThread);
                connectionThread.start();
            } catch (IOException ioe) {
                log.error("ListeningThread.run() is failed,erro is :",ioe);
            }
        }
    }

    /**
     * 关闭所有的socket客户端连接的线程
     */
    public void stopRunning() {
        for (ConnectionThread connectionThread : socketServer.getExistConnectionThreadList()) {
            connectionThread.stopRunning();
        }
        isRunning = false;
    }
}
