package com.socket.springbootsocket.socket.handler;

import com.socket.springbootsocket.socket.dto.MessageDto;
import com.socket.springbootsocket.socket.server.Connection;

/**
 * 处理消息的接口
 * @author 超
 * Create by fengc on  4/28/2023 14:35
 */
public interface MessageHandler {

    /**
     * 获得消息的处理函数
     * @param connection 封装了客户端的socket
     * @param messageDto 接收到的消息
     */
    void onReceive(Connection connection, MessageDto messageDto);

}
