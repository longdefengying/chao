package com.socket.springbootsocket.socket.server;

import com.socket.springbootsocket.socket.constants.SocketConstant;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;

/**
 * @author 超
 * Create by fengc on  4/28/2023 14:36
 */
@Slf4j
@Data
public class Connection {
    /**
     * 当前的socket的连接实例
     */
    private Socket socket;
    /**
     * 当前连接线程
     */
    private ConnectionThread connectionThread;

    /**
     * 当前连接是否登录
     */
    private boolean isLogin;

    /**
     * 存储当前的 userId
     */
    private String userId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 最后一次更新时间
     */
    private Date lastOnTime;

    public Connection(Socket socket, ConnectionThread connectionThread) {
        this.socket = socket;
        this.connectionThread = connectionThread;
    }

    public void println(String message) {
        int count = 0;
        PrintWriter writer;
        do {
            try {
                writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()),true);
                writer.println(message);
            } catch (IOException e) {
                count ++;
                if (count >= SocketConstant.RETRY_COUNT) {
                    this.connectionThread.stopRunning();
                }
            }
            try {
                Thread.sleep(2 * 1000);
            } catch (InterruptedException e) {
                log.error("Connection.println..userId={}.error,",userId,e);
            }
        } while (count < 3);
    }
}
