package com.socket.springbootsocket.socket.enums;

/**
 * @author 超
 * Create by fengc on  3/15/2023 19:31
 */
public enum FunctionCodeEnum {
    HEART(0,"心跳"),
    LOGIN(1,"登录"),
    CLOSE(2,"客户端关闭"),
    MESSAGE(3,"发送消息")
    ;

    private Integer code;

    private String desc;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    FunctionCodeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
