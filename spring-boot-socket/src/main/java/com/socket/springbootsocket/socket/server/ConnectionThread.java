package com.socket.springbootsocket.socket.server;

import com.alibaba.fastjson.JSONObject;
import com.socket.springbootsocket.socket.constants.SocketConstant;
import com.socket.springbootsocket.socket.dto.MessageDto;
import com.socket.springbootsocket.socket.enums.FunctionCodeEnum;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Calendar;
import java.util.Date;

/**
 * 每一个client连接开一个线程
 * @author 超
 * Create by fengc on  4/28/2023 14:40
 */
@Slf4j
@Data
public class ConnectionThread extends Thread{
    /**
     * 客户端
     */
    private Socket socket;

    /**
     * 服务端Socket
     */
    private SocketServer socketServer;

    /**
     * 封装客户端连接socket
     */
    private Connection connection;

    /**
     * 判断当前连接是否运行
     */
    private boolean isRunning;

    public ConnectionThread(Socket socket, SocketServer socketServer) {
        this.socket = socket;
        this.socketServer = socketServer;
        connection = new Connection(socket,this);
        connection.setCreateTime(Calendar.getInstance().getTime());
        connection.setLastOnTime(Calendar.getInstance().getTime());
        isRunning = true;
    }

    @Override
    public void run() {
        while (isRunning) {
            if (socket.isClosed()) {
                isRunning = false;
                break;
            }
            BufferedReader reader;
            try {
                reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String message;
                while ((message = reader.readLine()) != null) {
                    log.info("线程={},收到的消息={}",super.getName(),message);
                    MessageDto receiveDto ;
                    try {
                        receiveDto = JSONObject.parseObject(message,MessageDto.class);
                    } catch (Exception e) {
                        log.error("转换失败...,",e);
                        connection.println(JSONObject.toJSONString(
                                new MessageDto().setStatusCode(SocketConstant.STATUS_CODE_FAIL).setErrorMessage("格式错误")));
                        break;
                    }
                    //功能码
                    Integer functionCode = receiveDto.getFunctionCode();
                    //心跳类型
                    if (FunctionCodeEnum.HEART.getCode().equals(functionCode)) {
                        connection.setLastOnTime(Calendar.getInstance().getTime());
                        connection.println(JSONObject.toJSONString(
                                new MessageDto().setFunctionCode(FunctionCodeEnum.HEART.getCode())));
                    }
                    //登录身份验证
                    if (FunctionCodeEnum.LOGIN.getCode().equals(functionCode)) {
                        String userId = receiveDto.getUserId();
                        //验证已经登录的
                        if (socketServer.getLonginHandler().canLogin(userId)) {
                            connection.setLogin(true);
                            connection.setUserId(userId);
                            if (socketServer.getExistSocketMap().contains(userId)) {
                                Connection existConnection = socketServer.getExistSocketMap().get(userId);
                                existConnection.println(JSONObject.toJSONString(
                                        new MessageDto().setStatusCode(SocketConstant.STATUS_CODE_FAIL)
                                                .setErrorMessage("退出登录")
                                                .setFunctionCode(FunctionCodeEnum.MESSAGE.getCode())
                                ));
                                existConnection.getConnectionThread().stopRunning();
                            }
                            //添加到已经登录的map中
                            socketServer.getExistSocketMap().put(userId,connection);
                        } else {
                            //登录失败
                            connection.println(JSONObject.toJSONString(new MessageDto().setStatusCode(SocketConstant.STATUS_CODE_FAIL)
                                    .setFunctionCode(FunctionCodeEnum.MESSAGE.getCode()).setErrorMessage("登录验证失败")
                            ));
                            log.error("用户鉴权失败..userId={}",userId);
                        }
                    }
                    //发送消息
                    if (FunctionCodeEnum.MESSAGE.getCode().equals(functionCode)) {
                        //发送一些其他消息
                        socketServer.getMessageHandler().onReceive(connection,receiveDto);
                    }
                    //关闭
                    if (FunctionCodeEnum.CLOSE.getCode().equals(functionCode)) {
                        log.info("客户端主动退出登录...");
                        this.stopRunning();
                    }
                }
            } catch (Exception e) {
                log.error("运行的线程connectionThread.run() 失败，错误信息，",e);
                this.stopRunning();
            }
        }
    }
    public void stopRunning() {
        if (this.connection.isLogin()) {
            log.info("停止一个socket连接...ip={},userId={}",this.socket.getInetAddress().toString(),this.connection.getUserId());
        } else {
            log.info("停止一个没有身份验证的socket连接...ip={}",this.socket.getInetAddress().toString());
        }
        isRunning = false;
        try {
            socket.close();
        } catch (IOException ie) {
            log.error("关闭socket失败,原因：",ie);
        }
    }
}
