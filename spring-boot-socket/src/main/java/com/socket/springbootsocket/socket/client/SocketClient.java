package com.socket.springbootsocket.socket.client;

import com.alibaba.fastjson.JSONObject;
import com.socket.springbootsocket.socket.dto.MessageDto;
import com.socket.springbootsocket.socket.enums.FunctionCodeEnum;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.Socket;
import java.util.Date;

/**
 * @author 超
 * Create by fengc on  3/15/2023 15:36
 */
@Slf4j
@Data
public class SocketClient {

    private Socket socket;

    private Date lastOnTime;

    public SocketClient(String ip,int port) {
        try {
            socket = new Socket(ip,port);
            socket.setKeepAlive(true);
        } catch (Exception e) {
            log.error("connect socket error,",e);
        }
    }

    public void println(String message) {
        PrintWriter writer ;
        try {
            writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            writer.println(message);
        } catch (IOException e) {
            log.error("writer message error:",e);
        }
    }

    public String readLine() throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        return reader.readLine();
    }

    public void close() {
        try {
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            MessageDto receiveDto = new MessageDto();
            receiveDto.setFunctionCode(FunctionCodeEnum.CLOSE.getCode());
            writer.println(JSONObject.toJSONString(receiveDto));
        } catch (IOException e) {
            log.error("close fail :",e);
        }
    }

    public static void main(String[] args) {
        SocketClient socketClient = new SocketClient("127.0.0.1",60000);
        MessageDto messageDto = new MessageDto();
        messageDto.setFunctionCode(FunctionCodeEnum.LOGIN.getCode());
        messageDto.setUserId("client1");
        messageDto.setMessage("登陆上去了啦啦\n");
        socketClient.println(JSONObject.toJSONString(messageDto));
    }
}
