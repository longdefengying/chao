package com.socket.springbootsocket.socket.handler;

/**
 * @author 超
 * Create by fengc on  5/11/2023 20:12
 */
public interface LonginHandler {

    /**
     * client 登录的处理器
     * @param userId 用户ID
     * @return 是否登录通过
     */
    boolean canLogin(String userId);
}
