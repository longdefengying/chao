package com.socket.springbootsocket.controller;

import com.alibaba.fastjson.JSONObject;
import com.socket.springbootsocket.core.ResponseEntity;
import com.socket.springbootsocket.socket.server.Connection;
import com.socket.springbootsocket.socket.server.SocketServer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author 超
 * Create by fengc on  10/16/2023 11:42
 */
@RestController
@RequestMapping("/socket-server")
public class SocketServerController {

    @Resource
    private SocketServer socketServer;

    @GetMapping("/get/users")
    public ResponseEntity<JSONObject> getLoginUsers() {
        Map<String, Connection> userMap = socketServer.getExistSocketMap();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("total",userMap.keySet().size());
        jsonObject.put("dataList",userMap.keySet());
        return null;

    }
}
