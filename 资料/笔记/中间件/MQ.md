### RabbitMQ 的学习

>  **RabbitMQ**是实现了高级消息队列协议（AMQP）的开源消息代理软件（亦称面向消息的中间件）。RabbitMQ服务器是用[Erlang](https://baike.baidu.com/item/Erlang)语言编写的，而集群和故障转移是构建在[开放电信平台](https://baike.baidu.com/item/开放电信平台)框架上的。所有主要的编程语言均有与代理接口通讯的客户端[库](https://baike.baidu.com/item/库)。
>
> 主要的作用：异步、消峰、解耦



RabbitMQ的基本结构(消息的转发流程)：

​	![](D:\IdeaProjects\chao\资料\笔记\RabbitMQ结构图.jpg)

### 组成部分说明：

- Broker ：消息队列服务进程，此进程包括两个部分：Exchange 和 Queue.
- Exhcange : 消息队列交换机，按一定的规则顺序将消息路由转发到某个队列，对消息进行过滤.
- Queue ： RabbitMQ的内部对象，用于存储消息.
- Producer : 消息生产者，即生产客户端，生产客户端将消息发送.
- Consumer ： 消息消费者，即消费放客户端，接受MQ转发的消息.



## 订阅模型分类

1. 一个生产者多个消费者
2. 每个消费者都有一个自己的队列
3. 生产者没有将消息直接发送给队列，而是发送给Exchange(交换机、转发器)
4. 每个队列都需要绑定到交换机上
5. 生产者发送消息，经过交换机到达队列，实现一个消息被多个消费者消费

## 四种交换机

交换金有四种模式：

1. direct

   定向，将消息交给符合指定的 routing key的队列

2. fanout

   广播，将消息交给所有绑定交换机的队列

3. topic

   通配符，将消息交给符合 routing patter (路由模式) 的队列

4. header

   允许匹配 AMQP 消息的 header 而非路由键

   

## 六中消息类型

1. 基本消费模型

   一个生产者对应一个消费者！！！

   

2. work消费模型

   一个生产者对应多个消费者，但是只能有一个消费者获得消息！！！

   ```java
   void basicQos(int prefetchCount);
   设置 prefetchCount = 1,能者多劳 （会告诉RabbitMQ不要同时给一个消费者推送多于N个消息，即一旦有N个消息还没有ack，则该consumer将block掉，直到有消息ack）
   ```

   

3. Publish/Subscribe （交换机类型：Fanout(也称为广播)）

   一个消费者将消息首先发布到交换机，交换机绑定到多个队列，然后被监听该队列的消费者接收并消费。

   

   

4. Routing 路由模型（交换机类型：direct）

   ------

   

5. Topics 通配符模式 （交换机类型：topics）

   ------

   

6. RPC

