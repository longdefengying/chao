package mine;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author 超
 * Create by fengc on  2020/6/21 13:08
 */
public class TestClassPathXmlApplicationContext {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext();
		//System.out.println(context.getBean("fooService"));
		System.out.println("---------------------------------------------------------------------------------");
		System.out.println(context.getEnvironment().toString());
	}

}
