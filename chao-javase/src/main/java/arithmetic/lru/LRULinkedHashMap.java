package arithmetic.lru;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author 超
 * 利用LinkedHashMap 实现简单的缓存，必须实现 removeEldestEntry 的方法
 * Create by fengc on  2020/8/12 00:00
 */
public class LRULinkedHashMap<K,V> extends LinkedHashMap<K,V> {

    private final int maxCapaCity;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;
    private final Lock lock = new ReentrantLock();

    public LRULinkedHashMap(int maxCapaCity) {
        /**
         * 如果accessOrder为true的话，则会把访问过的元素放在链表后面，放置顺序是访问的顺序
         * 如果accessOrder为flase的话，则按插入顺序来遍历
         */
        super(maxCapaCity,DEFAULT_LOAD_FACTOR,true);
        this.maxCapaCity = maxCapaCity;
    }
    //必定实现此方法，因为默认是返回false。不移除元素
    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() > maxCapaCity;
    }

    @Override
    public boolean containsKey(Object key) {
        try {
            lock.lock();
            return super.containsKey(key);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public V get(Object key) {
        try {
            lock.lock();
            return super.get(key);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public V put(K key, V value) {
        try {
            lock.lock();
            return super.put(key, value);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public int size() {
        try {
            lock.lock();
            return super.size();
        } finally {
            lock.unlock();
        }
    }

    public void clear() {
        try {
            lock.lock();
            super.clear();
        } finally {
            lock.unlock();
        }
    }

    public Collection<Map.Entry<K,V>> getAll() {

        try {
            lock.lock();
            return new ArrayList<Map.Entry<K,V>>(super.entrySet());
        } finally {
            lock.unlock();
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Collection<Map.Entry<K,V>> CollectionEntry = getAll();
        Iterator<Map.Entry<K,V>> iterator = CollectionEntry.iterator();
        while (iterator.hasNext()) {
            Map.Entry<K,V> entry = iterator.next();
            sb.append(String.format("%s:%s ",entry.getKey(),entry.getValue()));
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        LRULinkedHashMap lrpMap = new LRULinkedHashMap(3);
        lrpMap.put(1,"a1");
        lrpMap.put(1,"a12");
        lrpMap.put(2,"a2");
        /*lrpMap.put(3,"a3");
        lrpMap.put(4,"a4");
        lrpMap.put(1,"a11");*/
        System.out.println(lrpMap.toString());
        lrpMap.get(3);
        System.out.println(lrpMap.toString());
        lrpMap.put(6,"a6");
        System.out.println(lrpMap.toString());




    }
}
