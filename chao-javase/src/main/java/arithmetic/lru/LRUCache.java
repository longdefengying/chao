package arithmetic.lru;

import java.util.HashMap;
import java.util.Objects;

/**
 * @author 超
 * Create by fengc on  2020/8/9 22:01
 * LRU 算法， HashMap + 链表实现
 * 采用头插法，取到的数或者从新插入的数据，都放到前面来。
 */
public class LRUCache<K,V> {

    //缓存最大的数
    private int cacheMaxSize;
    //存储链表的
    private HashMap<K,CacheNode> caches;
    //链表的第一个数
    private CacheNode<K,V> first;
    //链表的最后一个数
    private CacheNode<K,V> last;

    public LRUCache(int cacheMaxSize) {
        this.cacheMaxSize = cacheMaxSize;
        caches = new HashMap<>(cacheMaxSize);
    }

    public void put(K k, V v) {
        CacheNode<K,V> cacheNode = caches.get(k);
        if (Objects.isNull(cacheNode)) {
            if (caches.size() >= cacheMaxSize) {
                removeLast();
            }
            cacheNode = new CacheNode<>();
            cacheNode.k = k;
            cacheNode.v = v;

        }
        //把新加入的，插入到头部
        moveFirst(cacheNode);
        caches.put(k,cacheNode);
    }

    //通过key拿到Value,拿到的，标志为常用的，放到最前面
    public V get(K k) {
        CacheNode<K,V> node = caches.get(k);
        if (Objects.isNull(node)) {
            return null;
        }
        moveFirst(node);
        return node.v;
    }

    public void clear() {
        caches.clear();
        first = null;
        last = null;
    }

    //把当前的数插入链表前面去
    public void moveFirst(CacheNode<K,V> node) {
        //当是链表额第一个节点的时候，直接返回
        if (first == node) {
            return;
        }
        //当移动的是最后一个节点的时候，要把last设置为节点的前一个
        if (last == node) {
            last = node.pre;
        }
        //假如当前node的下一个指针不为空，则把当前node的下一个数的指向前一个数的指针，直接指向node的前一个数
        if (Objects.nonNull(node.next)) {
            node.next.pre = node.pre;
        }
        //同理
        if (Objects.nonNull(node.pre)) {
            node.pre.next = node.next;
        }
        //当第一个节点或者最后一个节点都是空的时候，那么此链表刚开始赋值，直接返回
        if (Objects.isNull(first) || Objects.isNull(last)) {
            first = last = node;
            return;
        }
        node.next = first;
        first.pre = node;
        first = node;
        first.pre = null;
    }

    //移除最后一个
    public void removeLast() {
        if (Objects.nonNull(last)) {
            CacheNode node = remove(last.k);
            last = node.pre;
            if (last == null) {
                first = null;
            } else {
                last.next = null;
            }
        }
    }
    //删除缓存中指定的key
    public CacheNode remove(K k) {
        CacheNode node = caches.get(k);
        if (Objects.nonNull(node)) {
            if (first == node) {
                first = node.next;
            }
            if (last == node) {
                last = node.pre;
            }
            if (Objects.nonNull(node.pre)) {
                node.pre.next = node.next;
            }
            if (Objects.nonNull(node.next)) {
                node.next.pre = node.pre;
            }
        }
        return caches.remove(k);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        CacheNode cacheNode = first;
        while(Objects.nonNull(cacheNode)) {
            sb.append(String.format("%s:%s ",cacheNode.k,cacheNode.v));
            cacheNode = cacheNode.next;
        }
        sb.append("}");
        return sb.toString();
    }

    class CacheNode<K,V> {
        K k;
        V v;
        //指向前一个数的指针
        CacheNode pre;
        //指向后一个数的指针
        CacheNode next;
    }

    public static void main(String[] args) {
        LRUCache<Integer,String> cache = new LRUCache(5);
        cache.put(1,"a1");
        cache.put(2,"a2");
        cache.put(3,"a3");
        cache.put(4,"a4");
        cache.put(5,"a5");
        cache.put(6,"a6");
        cache.put(1,"aa1");
        cache.get(3);
        System.out.println("3--->" + cache.toString());
        cache.put(9,"a9");
        System.out.println("9--->" + cache.toString());
        cache.get(6);
        System.out.println("6--->" + cache.toString());
        cache.put(7,"a7");
        System.out.println("7--->" + cache.toString());
        cache.put(8,"a8");
        System.out.println("8--->" + cache.toString());

    }

}
