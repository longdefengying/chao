package thread.threadlocal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 超
 * Create by fengc on  2020/8/22 13:43
 * 日期转化工具类
 */
public class ThreadLocalDateUtils {
    /**
     * Java8 直接初始化，lambam写法
     */
    private final static ThreadLocal<SimpleDateFormat> ymdSimpleFormatThrread = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd"));

    private final static ThreadLocal<SimpleDateFormat> simpleFormatThread = new ThreadLocal<SimpleDateFormat>(){
        @Override
        protected SimpleDateFormat initialValue() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            System.out.println(Thread.currentThread().getName() + "<-> " + simpleDateFormat);
            return simpleDateFormat;
        }
    };

   /*
   错误
   static {
       simpleFormatThread = new ThreadLocal<>();
       SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       System.out.println("static<-->" + simpleDateFormat);
       simpleFormatThread.set(simpleDateFormat);
   }*/

    /**
     * 日期转为字符串
     * @param date
     * @return
     */
    public static String dateToStr(Date date) {
        SimpleDateFormat simpleDateFormat = simpleFormatThread.get();
        System.out.println("-->" + simpleDateFormat);
        return simpleDateFormat.format(date);
    }

    /**
     * 字符串转为日期
     * @param dateStr
     * @return
     * @throws ParseException
     */
    public static Date strToDate(String dateStr) throws ParseException {
        SimpleDateFormat simpleDateFormat = simpleFormatThread.get();
        Date date = null;
        try {
            date = simpleDateFormat.parse(dateStr);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return date;
    }
}
