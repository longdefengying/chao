package thread.threadlocal;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author 超
 * Create by fengc on  2020/8/23 00:00
 * 以自己喜欢的key存入某个自己想要的对象到ThreadLocal中去
 */
public class ThreadLocalMapUtil<T> {

    //创建一个ThreadLocalMap
    private static final ThreadLocal<Map<String,Object>> threadLcoal = ThreadLocal.withInitial(HashMap::new);
    //直接拿出Map
    public static Map<String,Object> getThreadLocalMap() {
        return threadLcoal.get();
    }

    /**
     * 通过key拿出对象
     * @param key
     * @param <T>
     * @return
     */
    public static <T> T get(String key) {
        Map<String,Object> map = threadLcoal.get();
        @SuppressWarnings("unchecked")
        T t = (T)map.get(key);
        return t;
    }

    /**
     * 获取不到返回默认值
     * @param key
     * @param defaulValue
     * @param <T>
     * @return
     */
    public static <T> T get(String key,T defaulValue) {
        Map<String,Object> map = threadLcoal.get();
        @SuppressWarnings("unchecked")
        T t = Objects.nonNull(map) ? (T)map.get(key) : defaulValue;
        return t;
    }

    /**
     * 设置值
     * @param key
     * @param value
     */
    public static void set(String key,Object value) {
        Map<String,Object> map = threadLcoal.get();
        map.put(key,value);
    }

    /**
     * 直接设置一个Map
     * @param map
     */
    public static void set(Map<String,Object> map) {
        threadLcoal.get().putAll(map);
    }

    /**
     * 移除当前的
     */
    public static void remove() {
        threadLcoal.remove();
    }
}
