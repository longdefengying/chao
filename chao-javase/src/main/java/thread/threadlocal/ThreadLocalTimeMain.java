package thread.threadlocal;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author 超
 * Create by fengc on  2020/8/22 12:35
 * 应用场景：
 * 每个线程都需要一个独立的对象
 * （比如工具类，典型的就是SimpleDateFormat，
 * 每次使用都需要new一次，很浪费性能，直接放到成员变量里又是线性不安全，所以直接放到ThreadLocal 管理）
 */
public class ThreadLocalTimeMain {

    private static final ExecutorService executorService = Executors.newFixedThreadPool(2);

    private final static AtomicInteger countAtomic = new AtomicInteger();

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            int finalI = i;
            executorService.execute(() -> {

                String dateStr = ThreadLocalDateUtils.dateToStr(new Date(System.currentTimeMillis() + finalI*100));
                //查看输出线程，
                System.out.println("计数器" +countAtomic.incrementAndGet()+"，-> " + Thread.currentThread().getName()+":" + dateStr);
            });
        }
        executorService.shutdown();
    }
}
