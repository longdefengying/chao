package thread.threadlocal;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author 超
 * Create by fengc on  2020/8/22 23:44
 * 测试线性安全问题
 */
public class ThreadLocalTest {

    private final static ExecutorService exec = Executors.newFixedThreadPool(2);
    Integer num = 0;
    static Integer staticNum = 0;

    public static void main(String[] args) {
        ThreadLocalTest test = new ThreadLocalTest();
        for (int i = 0; i < 100 ; i++) {
            exec.execute(() -> {
                new ThreadLocalTest().caculator(1);
            });
        }
        exec.shutdown();
    }

    public void caculator(int number) {
        num = num + number;
        staticNum = staticNum + number;
        try {
            Thread.sleep(80);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("num=" + num + "--->staticNum=" + staticNum);
    }
}
