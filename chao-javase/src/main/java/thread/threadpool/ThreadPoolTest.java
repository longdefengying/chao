package thread.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @Author: fengchao
 * @Date: 2020/11/2 20:05
 * @Version 1.0
 * @decription todo
 */
public class ThreadPoolTest {

    private static ExecutorService executorService = Executors.newFixedThreadPool(10);

    public static void main(String[] args) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                
            }
        });
    }
}
