package thread.future;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author 超
 * Create by fengc on  2021/8/4 19:56
 */
public class FutureTaskTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<Integer> funture = new FutureTask<>(() -> {
            System.out.println("running.......");
            Thread.sleep(2000);
            return 1000;
        });

        Thread t1 = new Thread(funture,"t1");
        t1.start();

        System.out.println(funture.get());

    }
}
