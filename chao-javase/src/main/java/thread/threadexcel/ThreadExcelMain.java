package thread.threadexcel;

import com.mysql.cj.util.StringUtils;
import jdbc.JdbcUtils;

import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author: fengchao
 * @Date: 2020/10/30 15:14
 * @Version 1.0
 * @decription todo
 */
public class ThreadExcelMain {
    /**
     * 获取系统处理器个数，作为线程池数量
     */
    private static int nThreads = Runtime.getRuntime().availableProcessors();
    /**
     * 手动创建一个线程池
     *  corePoolSize：线程池中核心线程数的最大值
     *  maximumPoolSize：线程池中能拥有最多线程数
     *  keepAliveTime 表示空闲线程的存活时间。
     *  unit 表示keepAliveTime的单位。
     *  workQueue  用于缓存任务的阻塞队列
      */
    private static ExecutorService executorService = new ThreadPoolExecutor(10,10,60L,TimeUnit.SECONDS,new ArrayBlockingQueue<>(10));

    private static AtomicInteger atomicInteger = new AtomicInteger(1);
    private static AtomicInteger threadInteger = new AtomicInteger(1);

    private static BlockingQueue<String> queue = new LinkedBlockingDeque<>();

    private static Integer pageSize = 3000;

    public static void main(String[] args) {
        System.out.println("系统处理器个数=" + nThreads);

        sychronData(nThreads);
    }

    public static void sychronData(int nThreads) {
        for (int i = 0 ; i < nThreads; i++) {
             new Thread(){
                @Override
                public void run() {
                    boolean isFlag = false;
                    while (!isFlag) {
                        int pageNo = (atomicInteger.get() - 1) * pageSize;
                        System.out.println("---------------------------线程为："+Thread.currentThread().getName()+",pageNo=" + pageNo + "index=" + atomicInteger.get());
                        String sql = "SELECT id FROM xxl_job_log ORDER BY id ASC LIMIT " + pageNo + "," + pageSize;
                        String result = JdbcUtils.resultSetToJson(JdbcUtils.queryByStatement(sql));
                        System.out.println("result size ="+ (StringUtils.isNullOrEmpty(result) ? 0 : result.length()));
                        if (!StringUtils.isNullOrEmpty(result)) {
                            while (atomicInteger.get() == threadInteger.get()) {
                                try {
                                    queue.put(result);
                                    threadInteger.incrementAndGet();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            isFlag = true;
                        }
                        atomicInteger.incrementAndGet();
                    }
                }
            }.start();
        }
        new Thread(){
            @Override
            public void run() {
                try {
                    boolean isFlag = false;
                    File file = new File("E:\\demo\\xx-job-log.txt");
                    FileWriter writer = new FileWriter(file);
                    while (!isFlag) {
                        //System.out.println("queue 的size = "+queue.size());
                        String str = queue.poll(20,TimeUnit.SECONDS);
                       // System.out.println("queue 的size = "+queue.size()+",str 的size=" + str.length());
                        if (StringUtils.isNullOrEmpty(str)) {
                            isFlag = true;
                        } else {
                            writer.write(str + "\r\n");
                            writer.flush();
                        }
                    }
                    writer.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }

}
