package thread.sync;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author ��
 * Create by fengc on  2020/11/18 23:15
 */
public class Test implements Runnable {

    private static volatile int i = 0;

    private Map<String,Integer> map = new ConcurrentHashMap();

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new Test());
        Thread t2 = new Thread(new Test());
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(i);
    }

    private synchronized void add() {
        i ++;
        if(map.get(Thread.currentThread().getName()) == null) {
            map.putIfAbsent(Thread.currentThread().getName(),1);
        }
        if (map.get(Thread.currentThread().getName()) != null) {
            System.out.println("name=" + Thread.currentThread().getName() + ",i=" + i);
        }
    }

    @Override
    public void run() {
        for (int i = 0 ; i < 100000 ; i++) {
            add();
        }
    }
}
