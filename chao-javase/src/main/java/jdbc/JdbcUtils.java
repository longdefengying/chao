package jdbc;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @Author: fengchao
 * @Date: 2020/10/30 16:34
 * @Version 1.0
 * @decription todo
 */
public class JdbcUtils {
    /**
     * 驱动
     */
    private static String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    /**
     * 创建数据库连接
     */
    private static String DB_URL = "jdbc:mysql://10.186.132.35:3306/utopa_test_pay_job?serverTimezone=GMT%2b8&useSSL=false&useUnicode=true&autoReconnect=true&characterEncoding=utf8&allowMultiQueries=true";

    private static String userName = "utopa_test";

    private static String password = "Q1@yQ@c*";

    //定义一个来接数据库对象的上下文环境变量
    private static Connection connection;
    //定义一个用于存储查询后的结果集的一个集合
    private static ResultSet resultSet;
    //创建一个statement通道
    private static Statement statement;
    //创建一个prepareStatement通道
    private static PreparedStatement preparedStatement;

    static {
        try {
            System.out.println("开始加载数据库驱动.....时间：" + LocalDate.now());
            Class.forName(JDBC_DRIVER).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取数据库连接
     */
    private static void getConnection() {
        try {
            connection = DriverManager.getConnection(DB_URL,userName,password);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }



    public static ResultSet queryByStatement(String sql){
        createStatement();
        try {
           // System.out.println(LocalDate.now()+"采用Statement方法执行sql查询语句。。。");
            resultSet = statement.executeQuery(sql);
        } catch (SQLException e) {
            System.out.println(LocalDate.now() +"采用Statement方法执行sql查询语句失败");
            e.printStackTrace();
        }
        return resultSet;
    }

    private static void createStatement() {
        getConnection();
        try {
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void closeAll() {
        if (resultSet != null) {
            try {
                System.out.println(LocalDate.now() + "关闭resultSet。。。");
                resultSet.close();
            } catch (SQLException e) {
                System.out.println(LocalDate.now() + "关闭resultSet异常。。。");
                e.printStackTrace();
            }
        }
        if (statement != null) {
            try {
                System.out.println(LocalDate.now() + "关闭statement。。。");
                statement.close();
            } catch (SQLException e) {
                System.out.println(LocalDate.now() + "关闭statement异常。。。");
                e.printStackTrace();
            }
        }
        if (preparedStatement != null) {
            try {
                System.out.println(LocalDate.now() + "关闭preparestatement。。。");
                preparedStatement.close();
            } catch (SQLException e) {
                System.out.println(LocalDate.now() + "关闭preparestatement异常。。。");
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                System.out.println(LocalDate.now() + "关闭connection。。。");
                connection.close();
            } catch (SQLException e) {
                System.out.println(LocalDate.now() + "关闭connection异常。。。");
                e.printStackTrace();
            }
        }
    }

    public static String resultSetToJson(ResultSet resultSet)  {
        // json数组
        JSONArray jsonArray = new JSONArray();
        try {
            ResultSetMetaData metaData = resultSet.getMetaData();
            int columCount = metaData.getColumnCount();

            while (resultSet.next()) {
                JSONObject jsonObject = new JSONObject();
                for (int i = 1; i <= columCount; i++) {
                    String columName = metaData.getColumnName(i);
                    String value = resultSet.getString(columName);
                    jsonObject.put(columName, value);
                }

                jsonArray.add(JSONObject.toJSONString(jsonObject) + "");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (jsonArray.size() > 0) {
            return jsonArray.toJSONString();
        }
        return "";
    }

    public static void main(String[] args) throws SQLException {
        String sql = "SELECT * FROM xxl_job_qrtz_trigger_log LIMIT 10000";
        ResultSet resultSet = queryByStatement(sql);
        System.out.println(resultSetToJson(resultSet));
    }

 }
