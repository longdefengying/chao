package design.observe;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 超
 * Create by fengc on  2021/9/7 11:22
 */
public class OrderTest {
    public static void main(String[] args) {
        OrderSubject subject = new BusinessOrderSubject();
        OrderObserver observer1 = new BusinessOrder1();
        OrderObserver observer2 = new BusinessOrder2();
        OrderObserver observer3 = new BusinessOrder3();
        subject.register(observer1);
        subject.register(observer2);
        subject.register(observer3);
        subject.notify(3);
    }
}
//主体接口
interface OrderSubject {
    List<OrderObserver> list = new ArrayList<>();
    default void register(OrderObserver observer){
        list.add(observer);
    }
    void notify(int status);
}

class BusinessOrderSubject implements OrderSubject {
    @Override
    public void notify(int status) {
        for (OrderObserver observer : list) {
            observer.update(status);
        }
    }
}

//抽象观察
interface OrderObserver {
    void update(int status);
}

//
class BusinessOrder1 implements OrderObserver {
    @Override
    public void update(int status) {
        System.out.println("-----BusinessOrder1=" + status);
    }
}

class BusinessOrder2 implements OrderObserver {
    @Override
    public void update(int status) {
        System.out.println("-----BusinessOrder2=" + status);
    }
}

class BusinessOrder3 implements OrderObserver {
    @Override
    public void update(int status) {
        System.out.println("-----BusinessOrder3=" + status);
    }
}
