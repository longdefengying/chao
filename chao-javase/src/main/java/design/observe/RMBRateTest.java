package design.observe;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 超
 * Create by fengc on  2021/9/2 11:55
 * 例子：
 * 利用观察者模式设计一个程序，分析“人名币汇率”的升值或者贬值对进口公司进口产品成本或者出口公司的出口产品收入及公司利润率的影响
 * 当 ”人名币利率“升值的是时候，进口公司的进口产品的成本降低了且利润提率升了，出口公司的出口产品收入降低且利润率降低；
 */
public class RMBRateTest {

    public static void main(String[] args) {
        Rate rate = new RMBRate();
        Company c1 = new ImportCompany();
        Company c2 = new ExportCompany();
        rate.add(c1);
        rate.add(c2);
        rate.change(5);
        rate.change(-5);

    }

}

//抽象目标接口
interface Rate {
    List<Company> companys = new ArrayList<>();
     default void add(Company company) {
         companys.add(company);
     }
     default void remove(Company company) {
         companys.remove(company);
     }
    //通知观察者方法
     void change(int number);
}

//具体目标
class RMBRate implements Rate {
    @Override
    public void change(int number) {
        for (Company company : companys) {
            company.response(number);
        }
    }
}

//抽象观察者 ：公司
interface Company {
    void response(int number);
}

//具体观察者：进口公司
class ImportCompany implements Company {
    @Override
    public void response(int number) {
        if (number > 0) {
            System.out.println("人名币升值了：" + number + "个基点，进口公司降低了进口的成本");
        } else if (number < 0) {
            System.out.println("人名币贬值了：" + number + "个基点，进口公司提升了进口的成本");
        }
    }
}

//具体观察者：出口公司
class ExportCompany implements Company {
    @Override
    public void response(int number) {
        if (number > 0) {
            System.out.println("人名币升值了：" + number + "个基点，出口公司的出口产产品降低了收入");
        } else if (number < 0) {
            System.out.println("人名币贬值了：" + number + "个基点，出口公司的出口产品提升了收入");
        }
    }
}



