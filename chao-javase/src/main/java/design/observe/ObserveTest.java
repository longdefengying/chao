package design.observe;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 超
 * Create by fengc on  2021/8/10 11:47
 *简单的观察者模式
 *
 * 1、抽象主题角色（Subject）：也叫抽象目标类，它提供一个用于保存观察者对象的聚集类和增加、
 * 删除观察者对象的方法、以及通知所有观察者的抽象方法；
 *
 * 2、具体主题角色(Concrete Subject)：也叫具体目标类，它实现抽象目标的通知方法，
 * 当具体主题的内部状态发生改变的时，通知所有注册过的管擦者对象
 *
 * 3、抽象观察者角色(Observer)：它是一个抽象类或者接口，
 * 它包含了一个更新自己的抽象方法，当接到具体主题的更改通知时被调用。
 *
 * 4、具体观察者角色(Concrete Observer)：实现抽象观察者中定义的抽象方法，以便在得到目标的更改通知时更新自身的状态。
 *
 */
public class ObserveTest {

    public static void main(String[] args) {
        Subject sub = new ConcreteSubject();
        Observer o1 = new ConcreteObserveOne();
        Observer o2 = new ConcreteObserveTwo();
        sub.add(o1);
        sub.add(o2);
        sub.notifyObserver();
    }

}

//抽象目标
interface Subject {
    List<Observer> observers = new ArrayList<>();

    //增加观察者方法
    default void add(Observer observer) {
        observers.add(observer);
    }
    //删除观察者方法
    default void remove(Observer observer) {
        observers.remove(observer);
    }
    //通知观察者
    public void notifyObserver();
}

class ConcreteSubject implements Subject {
    @Override
    public void notifyObserver() {
        System.out.println("具体目标发生改变.....");
        System.out.println("------------------");
        for (Object o : observers) {
            ((Observer)o).response();
        }
    }
}

//抽象观察者
interface Observer {
    //反应方法
    void response();
}

//具体观察者角色1
class ConcreteObserveOne implements Observer {
    @Override
    public void response() {
        System.out.println("具体观察者角色1作出反应.......");
    }
}

//具体观察者角色2
class ConcreteObserveTwo implements Observer{
    @Override
    public void response() {
        System.out.println("具体观察者角色2作出反应........");
    }
}
