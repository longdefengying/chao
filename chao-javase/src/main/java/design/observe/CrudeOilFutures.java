package design.observe;

import java.util.Observable;
import java.util.Observer;

/**
 * @author 超
 * Create by fengc on  2021/9/2 23:37
 */
public class CrudeOilFutures {

    public static void main(String[] args) {
        OilFutures futures = new OilFutures();
        Observer bull = new Bull();
        Observer bear = new Bear();
        futures.addObserver(bull);
        futures.addObserver(bear);
        futures.setPrice(9);
        futures.setPrice(-9);
    }
}

//具体目标 : 原油期货
class OilFutures extends Observable {
    private float price;

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        //设置内部标志位
        super.setChanged();
        //通知油价变了
        super.notifyObservers(price);
        this.price = price;
    }
}

//具体管擦者：多方
class Bull implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        float price = ((Float)arg).floatValue();
        if (price > 0) {
            System.out.println("油价上涨" + price + "元，多方高兴了!");
        } else if (price < 0) {
            System.out.println("油价下跌" + price + "元，多方伤心了!");
        }
    }
}
//具体管擦者：空方
class Bear implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        float price = ((Float)arg).floatValue();
        if (price > 0) {
            System.out.println("油价上涨" + price + "元，空方伤心了!");
        } else if (price < 0) {
            System.out.println("油价下跌" + price + "元，空方高兴了!");
        }
    }
}


