package design.observe;

import java.util.*;

/**
 * @author 超
 * Create by fengc on  2021/9/2 19:21
 */
public class BellEventTest {
    public static void main(String[] args) {
        BellEventSource source = new BellEventSource();
        BellEventListener teach = new TeachEventListener();
        BellEventListener stu = new StuEventListener();
        source.addPersonListener(teach);
        source.addPersonListener(stu);
        source.ring(true);
        System.out.println("-----------------------------------");
        source.ring(false);

    }
}

/**
 * 铃声事件类：用于封装时间源以及一些与时间相关的参数
 */
class RingEvent extends EventObject {

    private boolean sound;

    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public RingEvent(Object source, boolean sound) {
        super(source);
        this.sound = sound;
    }

    public boolean getSound() {
        return sound;
    }

    public void setSound(boolean sound) {
        this.sound = sound;
    }
}

//目标类：事件源，铃
class BellEventSource {
    //监听容器
    private List<BellEventListener> listenerList;

    public BellEventSource() {
        this.listenerList = new ArrayList<>();
    }

    public void addPersonListener(BellEventListener bellEventListener) {
        listenerList.add(bellEventListener);
    }

    public void ring(boolean sound) {
        System.out.println(sound ? "上课铃响" : "下课铃响");
        RingEvent event = new RingEvent(this,sound);
        notifies(event);
    }

    void notifies(RingEvent event) {
        Iterator<BellEventListener> iterator = listenerList.iterator();
        while (iterator.hasNext()) {
            iterator.next().hearBell(event);
        }
    }
}


//抽象观察类：铃声时间监听器
interface BellEventListener extends EventListener {
    //时间处理方法，听到铃声
    public void hearBell(RingEvent ringEvent);
}

//具体观察者类：老师事件监听器
class TeachEventListener implements BellEventListener {
    @Override
    public void hearBell(RingEvent ringEvent) {
        if (ringEvent.getSound()) {
            System.out.println("====老师上课了====");
        } else {
            System.out.println("=====老师下课了====");
        }
     }
}
//具体观察类：学生事件监听器
class StuEventListener implements BellEventListener {
    @Override
    public void hearBell(RingEvent ringEvent) {
        if (ringEvent.getSound()) {
            System.out.println("====学生上课了====");
        } else {
            System.out.println("====学生下课了====");
        }
    }
}