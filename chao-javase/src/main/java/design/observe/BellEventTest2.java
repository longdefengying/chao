package design.observe;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 超
 * Create by fengc on  2021/9/2 23:03
 * 设置学校铃声、老师、学生】
 * 观察者模式
 */
public class BellEventTest2 {

    public static void main(String[] args) {
        BellEventSouce souce = new BellEventSouce();
        BellRingListener teach = new TeachBellRingListener();
        BellRingListener stu = new StuBellRingListener();
        souce.addListener(teach);
        souce.addListener(stu);
        souce.ring(true);
        souce.ring(false);

    }
}

class BellEventSouce {
    private List<BellRingListener> ringListenerList;

    public BellEventSouce() {
        this.ringListenerList = new ArrayList<>();
    }

    public void addListener(BellRingListener listener) {
        ringListenerList.add(listener);
    }

    public void ring(boolean sound) {
        System.out.println(sound ? "上课铃响了" : "下课铃响了");
        BellEvent event = new BellEvent(sound);
        ringListenerList.stream().forEach(rl -> {
            rl.heardBell(event);
        });
    }
}

//事件属性，设置铃声
class BellEvent {

    private boolean sound;

    public BellEvent(boolean sound) {
        this.sound = sound;
    }

    public boolean isSound() {
        return sound;
    }

    public void setSound(boolean sound) {
        this.sound = sound;
    }
}

//抽象观察者
interface BellRingListener {
    void heardBell(BellEvent event);
}

//目标管擦者
class TeachBellRingListener implements BellRingListener{
    @Override
    public void heardBell(BellEvent event) {
        if (event.isSound()) {
            System.out.println("老师要开始上课了....");
        } else {
            System.out.println("老师准备下课了....");
        }
     }
}
//目标管擦者
class StuBellRingListener implements BellRingListener {
    @Override
    public void heardBell(BellEvent event) {
        if (event.isSound()) {
            System.out.println("学生要上课了........");
        } else {
            System.out.println("学生下课了........");
        }
    }
}
