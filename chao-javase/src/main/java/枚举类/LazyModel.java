package 枚举类;

/**
 * @Author: fengchao
 * @Date: 2020/7/21 19:34
 * @Version 1.0
 * @decription todo
 * 简单懒汉式
 */
public class LazyModel {

    private static LazyModel single = null;

    private LazyModel() {}

    public static LazyModel getInstance() {
        if (single == null) {
            single = new LazyModel();
        }
        return single;
    }
}
