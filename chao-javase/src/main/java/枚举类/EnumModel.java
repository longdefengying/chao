package 枚举类;

/**
 * @author 超
 * Create by fengc on  2020/7/21 23:53
 * 一个枚举类
 */
public enum EnumModel {

    INSTANCE;

    private String objName;


    public String getObjName() {
        return objName;
    }

    public void setObjName(String objName) {
        this.objName = objName;
    }
}
