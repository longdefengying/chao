package 枚举类;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

/**
 * @author 超
 * Create by fengc on  2020/7/21 23:19
 * 测试单例模式，是否真的保证是单例
 */
public class SingletonTest {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        System.out.println("1、反射机制对单例的破坏性");
        /**
         * 1、反射机制对单例的破坏性
         */
        Class clazz = DoubleLockModel.class;
        //得到该类已经声明的构造起
        Constructor[] constructors = clazz.getDeclaredConstructors();
        Arrays.asList(constructors).stream().forEach(cons -> {
            //把私有访问级别的设置为public
            cons.setAccessible(true);
            try {
               System.out.print("反射创建的实例与单例模式创建的实例比较(cons.newInstance() == DoubleLockModel.getInstance())：");
               System.out.println(cons.newInstance() == DoubleLockModel.getInstance() );
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        });
        System.out.println("2、序列化与反序列化对单例模式的破坏");
        /**
         * 2、序列化与反序列化对单例模式的破坏
         */
        //序列化类
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("singleton"));
        oos.writeObject(DoubleLockModel.getInstance());
        oos.close();
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("singleton"));
        DoubleLockModel doubleLockModel = (DoubleLockModel)ois.readObject();
        ois.close();
        System.out.print("序列化与反序列化的实例与单例模式的实例比较：");
        System.out.println(doubleLockModel == DoubleLockModel.getInstance());
    }

}
