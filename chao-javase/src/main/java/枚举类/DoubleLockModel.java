package 枚举类;

import java.io.Serializable;

/**
 * @Author: fengchao
 * @Date: 2020/7/21 19:36
 * @Version 1.0
 * @decription todo 双重模式锁单例模式
 */
public class DoubleLockModel implements Serializable {

    private static DoubleLockModel single = null;

    private DoubleLockModel() {}

    public static DoubleLockModel getInstance() {
        if (single == null) {
            synchronized (DoubleLockModel.class) {
                if (single == null) {
                    single = new DoubleLockModel();
                }
            }
        }
        return single;
    }
}
