package 枚举类;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author 超
 * Create by fengc on  2020/7/21 23:55
 * 测试枚举类单例模式是否能破坏
 */
public class EnumTest {

    public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("enumTest"));
        oos.writeObject(EnumModel.INSTANCE);
        oos.close();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("enumTest"));
        EnumModel enumModel = (EnumModel) ois.readObject();
        ois.close();

        System.out.print("枚举类的序列化对象与枚举类对比：");
        System.out.println(EnumModel.INSTANCE == enumModel);

        Class clazz = EnumModel.class;
        Constructor constructor = clazz.getDeclaredConstructor();
        constructor.setAccessible(true);
        EnumModel enumModel1 = (EnumModel) constructor.newInstance();
        System.out.print("枚举的反射对象与枚举类对比：");
        System.out.println(EnumModel.INSTANCE == enumModel1);
    }

}
