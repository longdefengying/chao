package functionnal;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author 超
 * Create by fengc on  10/9/2023 23:29
 */
public class MyServiceMain {
    public static void main(String[] args) {
        MyService service = message -> System.out.println("Hello," + message);
        service.sayMessage("functionalInterface!");
        //Supplier 接口代表一个供应商，它不接受任何参数，但是返回一个值
        Supplier<String> supplier = () -> "Hello";
        String message = supplier.get();
        System.out.println("message=" + message);
        //Consumer 接口代表消费者，它接受一个参数但是不返回任何值
        Consumer<String> consumer = msg -> System.out.println(msg);
        consumer.accept("Consumer!!!");
        //Function<T,R> 接口代表一个函数，它接受一个参数并且返回一个值
        Function<Integer,Integer> square = x -> x * x;
        int result = square.apply(5);
        System.out.println("Function : " + result);
    }
}
