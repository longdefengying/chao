package lambda;

import javax.swing.*;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

/**
 * @author 超
 * Create by fengc on  2021/4/15 20:39
 * Lanmbda
 * 小例子
 */
public class LambdaTest {

    public static void main(String[] args) {
        var planets = new String[]{"Mercury","Venus","Earth","Mars","Jupiter","Saturn","Uranus","Neptune"};
        Arrays.sort(planets,String::compareToIgnoreCase);
        System.out.println(Arrays.toString(planets));
        System.out.println("Sorted in dictionary order:");
        Arrays.sort(planets);
        System.out.println(Arrays.toString(planets));
        System.out.println("Sorted in length:");
        Arrays.sort(planets,(A,B) -> A.length() - B.length());
        System.out.println(Arrays.toString(planets));
        var timer = new Timer(1000,event -> System.out.println("The time is " + new Date()));
        timer.start();
        JOptionPane.showMessageDialog(null,"Quit program?");
        System.exit(0);

       /* LocalDate day = null;
        //1、不管day是否为空，后面参数都是会生成一个预备的对象
        LocalDate hirday = Objects.requireNonNullElse(day,LocalDate.of(1970,2,1));
        System.out.println("hirday=" + hirday);

        //2、后面的lambda，表示是延迟计算，只有当day是null的时候，才会生成预备对象
        LocalDate hiryDay1 = Objects.requireNonNullElseGet(day,() -> LocalDate.of(1970,1,1));
        System.out.println("hirday=" + hirday);*/
    }

}
