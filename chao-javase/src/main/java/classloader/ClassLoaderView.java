package classloader;

/**
 * @author 超
 * Create by fengc on  2020/8/27 22:51
 * 查看类的加载器实例
 */
public class ClassLoaderView {

    public static void main(String[] args) throws Exception {

        System.out.println("核心类库加载器：" + ClassLoaderView.class.getClassLoader().loadClass("java.lang.String").getClassLoader());

        System.out.println("扩展类库加载器：" +
                ClassLoaderView.class.getClassLoader().loadClass("com.sun.imageio.spi.FileImageInputStreamSpi").getClassLoader());

        System.out.println("应用程序库加载器：" + ClassLoaderView.class.getClassLoader());

        System.out.println("应用程序库加载器的父亲:" + ClassLoaderView.class.getClassLoader().getParent());

        System.out.println("应用程序库加载器的父亲的父亲：" + ClassLoaderView.class.getClassLoader().getParent().getParent());
    }

}
