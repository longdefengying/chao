package time;

import java.time.DayOfWeek;
import java.time.LocalDate;

/**
 * @author 超
 * Create by fengc on  2021/3/25 00:08
 * 输出当月的日期
 * 当前日期：2021-03-25
 * Mon Tue Wed Thu Fri Sat Sun
 *   1   2   3   4   5   6   7
 *   8   9  10  11  12  13  14
 *  15  16  17  18  19  20  21
 *  22  23  24  25* 26  27  28
 *  29  30  31
 */
public class CalendarTest {

    public static void main(String[] args) {
        printCalendarMontDate();
    }

    public static void printCalendarMontDate() {
        //1、得到当前日期
        LocalDate date = LocalDate.now();
        System.out.println("当前日期：" + date);
        int month = date.getMonthValue();
        int today = date.getDayOfMonth();

        //2、得到当月的1号日期
        date = date.minusDays(today -1);
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        int value = dayOfWeek.getValue(); //得到1号当天的是星期几

        //3、输出格式
        System.out.println("Mon Tue Wed Thu Fri Sat Sun");
        for (int i = 1; i < value; i++) {
            System.out.print("  ");
        }

        //4、循环当月的日期，从1号开始加
        while (date.getMonthValue() == month) {
            System.out.printf("%3d",date.getDayOfMonth());
            //当增加到的日期是当天时候加上后缀 * 号
            if (date.getDayOfMonth() == today) {
                System.out.print("*");
            } else {
                System.out.print(" ");
            }
            //累加1=
            date = date.plusDays(1);
            //5、当累加到日期是星期1的时候，打印换行符
            if (date.getDayOfWeek().getValue() == 1) {
                System.out.println();
            }
        }
        //当时打印完这个月的最后一天不是星期一的时候，打印换行符
        if (date.getDayOfWeek().getValue() != 1) {
            System.out.println();
        }
    }

}
