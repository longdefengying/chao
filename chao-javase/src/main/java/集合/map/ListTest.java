package 集合.map;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ��
 * Create by fengc on  2020/10/23 00:54
 */
public class ListTest {

    public static void main(String[] args) {
        User user;
        List<User> userList = new ArrayList<>();
         for (int i = 0 ; i < 10 ; i++) {
             user = new User();
             user.setName("小明" + i);
             user.setSex(i % 2);
             userList.add(user);
             user = null;
         }
         System.out.println(userList);
    }


}

class User{
    private String name;

    private int sex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", sex=" + sex +
                '}';
    }
}
