package 集合.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: fengchao
 * @Date: 2020/7/21 19:59
 * @Version 1.0
 * @decription todo
 */
public class HashMapTest {

    Map map = new HashMap();

    List list = new ArrayList<>();

    public static void main(String[] args) {
        HashMap<Person, Integer> map = new HashMap<Person, Integer>();

        Person p = new Person("jack",22,"男");
        Person p1 = new Person("jack",22,"男");

        System.out.println("p的hashCode:"+p.hashCode());
        System.out.println("p1的hashCode:"+p1.hashCode());
        System.out.println(p.equals(p1));
        System.out.println(p == p1);

        map.put(p,888);
        map.put(p1,888);
        map.forEach((key,val)->{
            System.out.println(key);
            System.out.println(val);
        });

        System.out.println(1 << 30);
        System.out.println(1 >> 30);
        System.out.println(16 << 1);

        System.out.println(12 << 1);
        System.out.println(1 << 4);
        System.out.println(16 - (16 >>> 2));



    }
}

class Person
{
    private String name;

    private int age;

    private String sex;

    Person(String name,int age,String sex){
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof Person){
            Person person = (Person)obj;
            return name.equals(person.name);
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode()
    {
        return name.hashCode();
    }
}
