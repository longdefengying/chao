package clazz;

/**
 * @author 超
 * Create by fengc on  2021/12/28 19:49
 */
public class OutClassTest {

    public static long outTime = System.currentTimeMillis();

    static {
        System.out.println("外部静态化块加载，时间：" + System.currentTimeMillis());
    }

    public OutClassTest() {
            System.out.println("外部构造器加载，时间：" + System.currentTimeMillis());
    }

    static class InnerStaticClass {
        public static long innerStaticTime = System.currentTimeMillis();
        static {
            outTime = innerStaticTime;
            System.out.println("静态化内部类的静态化块加载，时间：" + outTime);
        }

        public InnerStaticClass() {
            outTime = System.currentTimeMillis();
            System.out.println("静态内部类构造器的加载，时间：" + outTime);
        }
    }

    class InnerClass {
        public InnerClass() {
            System.out.println("普通部类构造器的加载，时间：" + System.currentTimeMillis());
        }
    }

    public static void main(String[] args) {
        OutClassTest outClassTest = new OutClassTest();
        System.out.println("静态内部类的输出：" + InnerStaticClass.innerStaticTime);
        new OutClassTest.InnerStaticClass();
    }
}
