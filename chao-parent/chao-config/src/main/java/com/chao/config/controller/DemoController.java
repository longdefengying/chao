package com.chao.config.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 超
 * Create by fengc on  2021/6/10 00:01
 */
@RestController
@RequestMapping("/demo")
public class DemoController {

    @Value("${chao.datasource.name}")
    private String name;

    @Value("${chao.datasource.username}")
    private String username;

    @GetMapping("/findValue")
    public String findValue() {
        System.out.println("username:" + username);
        System.out.println("name:" + name);
        return name + username;
    }
}
