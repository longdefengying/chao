package com.central.common.swagger ;

import com.google.common.collect.Lists ;
import lombok.Data ;
import org.springframework.boot.context.properties.ConfigurationProperties ;
import org.springframework.stereotype.Component ;

import java.util.ArrayList ;
import java.util.LinkedHashMap;
import java.util.List ;
import java.util.Map;

/**
 * @author 超
 * Create by fengc on  2021/6/14 17:56
 */
@Data
@ConfigurationProperties("chao.swagger")
@Component
public class SwaggerProperties {
    /*是否开启swagger*/
    private Boolean enable ;
    /*标题*/
    private String title = "";
    /*描述*/
    private String description = "";
    /*版本*/
    private String version = "";
    /*许可证*/
    private String license = "";
    /*许可证URL*/
    private String licenscUrl = "";
    /*服务条框的URL*/
    private String termsOfServiceUrl = "";
    /*swagger会解释的包路径*/
    private String basePackage = "";
    private String host = "";
    /*联系人*/
    private Contact contact = new Contact();
    /*swagger会解释的url规则*/
    private List<String> basePath = Lists.newArrayList();
    /*basePaths 基础上需要排除的url规则*/
    private List<String> excludePath = Lists.newArrayList();
    /*分组文档*/
    private Map<String,DocketInfo> docketInfoMap = new LinkedHashMap<>();
    /*全局参数配置*/
    private List<GlobalOperationParameter> globalOperationParameters;

    @Data
    public static class Contact {
        /*联系人*/
        private String name = "";
        /*联系人的url*/
        private String url = "";
       /* 联系人的email*/
        private String email = "";
    }

    @Data
    public static class GlobalOperationParameter {
        /*参数名*/
        private String name = "";
        /*描述信息*/
        private String description = "";
        /*指定参数值类*/
        private String modelRef = "";
        /*参数放在哪个地方 header query path body form*/
        private String parameterType = "";
        /*参数是否必要传*/
        private String required = "";
    }

    @Data
    public static class DocketInfo {

        private String title = "";

        private String description = "";

        private String version = "";

        private String license = "";

        private String licenseUrl = "";

        /**服务条款URL**/
        private String termsOfServiceUrl = "" ;

        private Contact contact = new Contact();

        /**swagger会解析的包路径**/
        private String basePackage = "";

        /**swagger会解析的url规则**/
        private List<String> basePath = Lists.newArrayList();
        /**在basePath基础上需要排除的url规则**/
        private List<String> excludePath = Lists.newArrayList();
        
        private List<GlobalOperationParameter> globalOperationParameters = Lists.newArrayList();
    }
}
