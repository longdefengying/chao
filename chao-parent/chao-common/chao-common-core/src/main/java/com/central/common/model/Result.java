package com.central.common.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 超
 * Create by fengc on  2021/6/14 23:39
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> implements Serializable {

    private T date;
    private Integer respCode;
    private String respMsg;

    public static <T> Result<T> success(String respMsg) {
        return of(null,RespCodeEnum.SUCCESS.getCode(),respMsg);
    }

    public static <T> Result<T> success(T t, String respMsg) {
        return of(t,RespCodeEnum.SUCCESS.getCode(),respMsg);
    }

    public static <T> Result<T> success(T t) {
        return of(t,RespCodeEnum.SUCCESS.getCode(),"");
    }

    public <T> Result<T> fail(String respMsg) {
        return of(null,RespCodeEnum.ERROR.getCode(), respMsg);
    }

    public static <T> Result<T> of(T t,Integer respCode,String respMsg) {
        return new Result<>(t,respCode,respMsg);
    }
}
