package com.central.db.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author 超
 * Create by fengc on  2021/10/21 22:48
 * 父类
 */
public interface SupperMapper<T> extends BaseMapper<T> {

    //可以写一些公共的方法
}
