package com.central.db.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 超
 * Create by fengc on  2021/10/21 19:25
 */
@Setter
@Getter
@ConfigurationProperties(prefix = "chao.mybatis-plus.auto-fill")
//@RefrshScope
public class MybatisPlusAutoFillProperties {

    /**
     * 是否开启自动填充字段
     */
    private Boolean enable;
    /**
     * 是否开启插入填充
     */
    private Boolean enableInsertFill = true;
    /**
     * 是否开启自动更新
     */
    private Boolean enableUpdateFill = true;
    /**
     * 创建时间字段
     */
    private String createTimeField = "createTime";
    /**
     * 更新时间字段
     */
    private String updateTimeField = "updateTime";

}
