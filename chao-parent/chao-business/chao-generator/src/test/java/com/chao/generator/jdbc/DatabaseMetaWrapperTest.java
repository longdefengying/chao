package com.chao.generator.jdbc;

import cn.hutool.json.JSONUtil;
import com.chao.generator.config.DataSourceConfig;
import com.chao.generator.config.po.DataBaseMetaWrapper;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.Map;

/**
 * @author 超
 * Create by fengc on  2021/7/14 00:07
 */
public class DatabaseMetaWrapperTest {

    @Test
    void test() throws SQLException {
        DataSourceConfig dataSourceConfig =new DataSourceConfig.Builder("jdbc:mysql://127.0.0.1:3306/user-center?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&useSSL=false&zeroDateTimeBehavior=convertToNull&serverTimezone=Asia/Shanghai",
                "root", "root").build();
        DataBaseMetaWrapper dataBaseMetaWrapper = new DataBaseMetaWrapper(dataSourceConfig.getConn());
        Map<String,DataBaseMetaWrapper.ColumnsInfo> columnsInfoMap = dataBaseMetaWrapper.getColumnsInfo(null,null,"sys_user");
        System.out.println("=======================================");
        System.out.println(JSONUtil.toJsonStr(columnsInfoMap));
        System.out.println("=======================================");

    }
}
