package com.chao.generator.config.po;

import com.chao.generator.config.DataSourceConfig;
import com.chao.generator.config.GlobalConfig;
import com.chao.generator.config.builder.Entity;
import com.chao.generator.config.rules.IColumnType;
import org.apache.ibatis.type.JdbcType;

import java.util.Map;
import java.util.Objects;

/**
 * @author 超
 * Create by fengc on  2021/7/13 00:02
 * 表字段信息
 */
public class TableField {
    private boolean convert;
    private boolean keyFlag;
    /**
     * 主键是否自增
     */
    private boolean keyIdentityFlag;
    private String name;
    private String type;
    private String propertyName;
    private IColumnType columnType;
    private String comment;
    private String fill;
    /**
     * 是否关键字
     */
    private boolean keyWords;
    /**
     * 数据库字段（关键字转义符号）
     */
    private String clomnName;
    /**
     * 自定义查询字段列表
     */
    private Map<String,Object> customMap;
    /**
     * 字段元数据查询
     */
    private MetaInfo metaInfo;

   // private final Entity entity;

    private DataSourceConfig dataSourceConfig;

    private GlobalConfig globalConfig;

    public boolean isConvert() {
        return convert;
    }

    public void setConvert(boolean convert) {
        this.convert = convert;
    }

    public boolean isKeyFlag() {
        return keyFlag;
    }

    public void setKeyFlag(boolean keyFlag) {
        this.keyFlag = keyFlag;
    }

    public boolean isKeyIdentityFlag() {
        return keyIdentityFlag;
    }

    public void setKeyIdentityFlag(boolean keyIdentityFlag) {
        this.keyIdentityFlag = keyIdentityFlag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public IColumnType getColumnType() {
        return columnType;
    }

    public void setColumnType(IColumnType columnType) {
        this.columnType = columnType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getFill() {
        return fill;
    }

    public void setFill(String fill) {
        this.fill = fill;
    }

    public boolean isKeyWords() {
        return keyWords;
    }

    public void setKeyWords(boolean keyWords) {
        this.keyWords = keyWords;
    }

    public String getClomnName() {
        return clomnName;
    }

    public void setClomnName(String clomnName) {
        this.clomnName = clomnName;
    }

    public Map<String, Object> getCustomMap() {
        return customMap;
    }

    public void setCustomMap(Map<String, Object> customMap) {
        this.customMap = customMap;
    }

    public MetaInfo getMetaInfo() {
        return metaInfo;
    }

    public void setMetaInfo(MetaInfo metaInfo) {
        this.metaInfo = metaInfo;
    }

    public DataSourceConfig getDataSourceConfig() {
        return dataSourceConfig;
    }

    public void setDataSourceConfig(DataSourceConfig dataSourceConfig) {
        this.dataSourceConfig = dataSourceConfig;
    }

    public GlobalConfig getGlobalConfig() {
        return globalConfig;
    }

    public void setGlobalConfig(GlobalConfig globalConfig) {
        this.globalConfig = globalConfig;
    }

    /**
     * 元数据信息
      */
    public static class MetaInfo {
        private int length;

        private boolean nullable;

        private String remark;

        private String defaultValue;

        private int scale;

        private JdbcType jdbcType;

        public MetaInfo(DataBaseMetaWrapper.ColumnsInfo columnsInfo) {
            if (Objects.nonNull(columnsInfo)) {
                this.length = columnsInfo.getLength();
                this.nullable = columnsInfo.isNullable();
                this.remark = columnsInfo.getRemark();
                this.defaultValue = columnsInfo.getDefaultValue();
                this.scale = columnsInfo.getScale();
                this.jdbcType = columnsInfo.getJdbcType();

            }
        }

        public int getLength() {
            return length;
        }

        public void setLength(int length) {
            this.length = length;
        }

        public boolean isNullable() {
            return nullable;
        }

        public void setNullable(boolean nullable) {
            this.nullable = nullable;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getDefaultValue() {
            return defaultValue;
        }

        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        public int getScale() {
            return scale;
        }

        public void setScale(int scale) {
            this.scale = scale;
        }

        public JdbcType getJdbcType() {
            return jdbcType;
        }

        public void setJdbcType(JdbcType jdbcType) {
            this.jdbcType = jdbcType;
        }
    }
}
