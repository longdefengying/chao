package com.chao.generator.controller;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chao.generator.service.TableService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author 超
 * Create by fengc on  2021/6/15 00:17
 */
@Slf4j
@RestController
@RequestMapping("/generator")
public class GeneratorController {

    @Autowired
    private TableService tableService;

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String userName;

    @Value("${spring.datasource.password}")
    private String password;


    public IPage list() {

        return null;
    }

    @GetMapping("/name")
    public Object name() {
        JSONObject jsonObject = JSONUtil.createObj();
        jsonObject.accumulate("url",url);
        jsonObject.accumulate("userName",userName);
        jsonObject.accumulate("password",password);
        return jsonObject;
    }

    @GetMapping("/hello")
    public String hello() {

        return "say hello!";
    }

    @GetMapping("/code")
    public void generatorCode(String table, String moduleName, HttpServletResponse response) throws IOException {
        log.info("开始生成代码.........................table={},moduleName={}",table,moduleName);
        byte[] data = tableService.generatorCode(Lists.newArrayList(table),moduleName);
        response.reset();
        response.setHeader("Content-Disposition", "attachment; filename=\"generator.zip\"");
        response.addHeader("Content-Length", "" + data.length);
        response.setContentType("application/octet-stream; charset=UTF-8");
        IOUtils.write(data, response.getOutputStream());
    }

    @PostMapping("/code")
    public void generatorCodeByTables(@RequestBody Map<String,Object> requestMap) throws IOException {
        List<String> tables  = (List<String>)requestMap.get("tables");
        String moduleName = (String) requestMap.get("moduleName");
        log.info("开始生成代码.........................tables={},moduleName={}",tables,moduleName);
        byte[] data = tableService.generatorCode(tables,moduleName);
        FileOutputStream outputStream = new FileOutputStream(new File("F:\\file\\demo\\generator.zip"));
        IOUtils.write(data, outputStream);
        outputStream.close();
    }

}
