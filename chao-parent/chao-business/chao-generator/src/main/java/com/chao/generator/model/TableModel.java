package com.chao.generator.model;

import lombok.Data;

import java.util.List;

/**
 * @author 超
 * Create by fengc on  2021/10/20 16:41
 * 数据库表的详细信息实体类
 */
@Data
public class TableModel {

    /**
     * 表的名称
     */
    private String tableName;
    /**
     * 表的备注
     */
    private String comments;
    /**
     * 表的主键
     */
    private ColumnModel pk;
    /**
     * 表的列表名（不包含主键）
     */
    private List<ColumnModel> columns;
    /**
     *类名（第一个字母大写）
     */
    private String className;
    /**
     * 类名（第一个字母小写）
     */
    private String classname;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }
}
