package com.chao.generator.config;

import com.chao.generator.IConfigBuilder;
import org.apache.commons.lang.StringUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

/**
 * @author 超
 * Create by fengc on  2021/7/8 00:43
 * 数据库配置类
 */
public class DataSourceConfig {

    private String url;
    private String username;
    private String password;
    private Connection connection;
    private DataSource dataSource;
    private String schemaName;
    private IDbQuery dbQuery;


    public Connection getConn() {
        try {
            if (connection != null && !connection.isClosed()) {
                return connection;
            } else {
                synchronized (this) {
                    if (Objects.nonNull(dataSource)) {
                        connection = dataSource.getConnection();
                    } else {
                        this.connection = DriverManager.getConnection(url,username,password);
                    }
                }
            }
        } catch (SQLException throwables) {
           throw new RuntimeException(throwables);
        }
        return connection;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public IDbQuery getDbQuery() {
        return dbQuery;
    }

    public void setDbQuery(IDbQuery dbQuery) {
        this.dbQuery = dbQuery;
    }

    public static class Builder implements IConfigBuilder<DataSourceConfig> {

        private final DataSourceConfig dataSourceConfig;

        public Builder() {
            this.dataSourceConfig = new DataSourceConfig();
        }

        /**
         * 构造器，初始化数据库的连接配置
         * @param url
         * @param username
         * @param password
         */
        public Builder(String url, String username, String password) {
            this();
            if (StringUtils.isBlank(url)) {
                throw new RuntimeException("数据库的url不正确或者为空");
            }
            this.dataSourceConfig.url = url;
            this.dataSourceConfig.username = username;
            this.dataSourceConfig.password = password;
        }

        /**
         *构造方法
         * @param dataSource 外部数据源实例
         */
        public Builder(DataSource dataSource) {
            this();
            try {
                Connection connection = dataSource.getConnection();
                this.dataSourceConfig.username = connection.getMetaData().getUserName();
                this.dataSourceConfig.url = connection.getMetaData().getURL();
                this.dataSourceConfig.schemaName = connection.getSchema();
                this.dataSourceConfig.connection = connection;
            } catch (SQLException sqe) {
                throw new RuntimeException("构建数据库配置失败.",sqe);
            }
        }

        /**
         * 设置数据库的查询实现
         * @param dbQuery
         * @return
         */
        public Builder dbQuery(IDbQuery dbQuery) {
            this.dataSourceConfig.dbQuery = dbQuery;
            return this;
        }

        /**
         * 设置数据库的schema
         * @param schemaName
         * @return
         */
        public Builder schema(String schemaName) {
            this.dataSourceConfig.schemaName = schemaName;
            return this;
        }

        /**
         * 设置类转换器
         * @return
         */
        public Builder typeConvert() {

            return this;
        }




        @Override
        public DataSourceConfig build() {
            return this.dataSourceConfig;
        }
    }
}
