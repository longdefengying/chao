package com.chao.generator.dao;

import com.central.db.mapper.SupperMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author 超
 * Create by fengc on  2021/10/21 22:50
 */
@Mapper
public interface TableMapper extends SupperMapper {

    /**
     * 查询数据表的详细信息
     * @param tableName
     * @return
     */
    Map<String,String> selectTable(String tableName);

    /**
     * 查询数据库表的字段详情
     * @param tableName
     * @return
     */
    List<Map<String,String>> selectColumns(String tableName);
}
