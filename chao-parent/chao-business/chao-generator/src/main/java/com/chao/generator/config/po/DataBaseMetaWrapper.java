package com.chao.generator.config.po;

import org.apache.ibatis.type.JdbcType;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 超
 * Create by fengc on  2021/7/13 23:30
 */
public class DataBaseMetaWrapper {

    private final DatabaseMetaData databaseMetaData;

    public DataBaseMetaWrapper(Connection connection) throws SQLException {
        this.databaseMetaData = connection.getMetaData();
    }

    public Map<String,ColumnsInfo> getColumnsInfo(String catalog, String schemaPattern, String tableNamePattern) throws SQLException{
        ResultSet resultSet = databaseMetaData.getColumns(catalog,schemaPattern,tableNamePattern,"%");
        Map<String,ColumnsInfo> columnsInfoMap = new HashMap<>();
        while (resultSet.next()) {
            ColumnsInfo columnsInfo = new ColumnsInfo();
            String name = resultSet.getString("COLUMN_NAME");
            columnsInfo.name = name;
            columnsInfo.jdbcType = JdbcType.forCode(resultSet.getInt("DATA_TYPE"));
            columnsInfo.length = resultSet.getInt("COLUMN_SIZE");
            columnsInfo.scale = resultSet.getInt("DECIMAL_DIGITS");
            columnsInfo.remark = resultSet.getString("REMARKS");
            columnsInfo.defaultValue = resultSet.getString("COLUMN_DEF");
            columnsInfo.nullable = resultSet.getInt("NULLABLE") == DatabaseMetaData.columnNullable;
            columnsInfoMap.put(name.toLowerCase(),columnsInfo);
        }
        return Collections.unmodifiableMap(columnsInfoMap);
    }

    public static class ColumnsInfo {

        private String name;

        private int length;

        private boolean nullable;

        private String remark;

        private String defaultValue;

        private int scale;

        private JdbcType jdbcType;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getLength() {
            return length;
        }

        public void setLength(int length) {
            this.length = length;
        }

        public boolean isNullable() {
            return nullable;
        }

        public void setNullable(boolean nullable) {
            this.nullable = nullable;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getDefaultValue() {
            return defaultValue;
        }

        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        public int getScale() {
            return scale;
        }

        public void setScale(int scale) {
            this.scale = scale;
        }

        public JdbcType getJdbcType() {
            return jdbcType;
        }

        public void setJdbcType(JdbcType jdbcType) {
            this.jdbcType = jdbcType;
        }
    }
}
