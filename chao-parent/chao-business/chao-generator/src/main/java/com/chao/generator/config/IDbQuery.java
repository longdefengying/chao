package com.chao.generator.config;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author 超
 * 表数据查询接口
 * Create by fengc on  2021/7/12 23:37
 */
public interface IDbQuery {

    /**
     * 表信息查询SQL
     * @return
     */
    String tableSql();

    /**
     * 表字段信息查询sql
     * @return
     */
    String tableFiledSql();

    /**
     * 表名称
     * @return
     */
    String tableName();

    /**
     * 表注释
     * @return
     */
    String taleComment();

    /**
     * 字段名称
     * @return
     */
    String filedName();

    /**
     * 字段注释
     * @return
     */
    String filedComment();

    /**
     * 主键字段
     * @return
     */
    String filedKey();

    /**
     * 判断主键是否为identity
     * @param resultSet
     * @return
     * @throws SQLException
     */
    boolean isKeyIdentity(ResultSet resultSet) throws SQLException;

    /**
     * 自定义字段名称
     * @return
     */
    String[] fieldCustom();
}
