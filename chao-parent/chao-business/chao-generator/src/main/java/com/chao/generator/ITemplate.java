package com.chao.generator;

import com.chao.generator.config.po.TableInfo;

import java.util.Map;

/**
 * @author 超
 * Create by fengc on  2021/7/13 23:52
 */
public interface ITemplate {

    Map<String,Object> renderData(TableInfo tableInfo);

}
