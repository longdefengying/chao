package com.chao.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 超
 * Create by fengc on  2021/10/19 16:20
 */
@SpringBootApplication
public class ChaoGeneratorApp {

    public static void main(String[] args) {
        SpringApplication.run(ChaoGeneratorApp.class,args);
        System.out.println("--------------------------------启动成功............................................");
    }

}
