package com.chao.generator;

/**
 * @author 超
 * Create by fengc on  2021/7/8 00:41
 */
public interface IConfigBuilder<T> {

    T build();
}
