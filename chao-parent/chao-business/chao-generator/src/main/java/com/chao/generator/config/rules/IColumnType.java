package com.chao.generator.config.rules;

/**
 * @author 超
 * Create by fengc on  2021/7/12 23:56
 * 获取实体类字段属性类信息接口
 */
public interface IColumnType {
    /**
     * 获取字段类型
     * @return 字段类型
     */
    String getType();

    /**
     * 获取字段类型完整名
     * @return 字段类型完整名
     */
    String getPkg();
}
