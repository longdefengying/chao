package com.chao.generator.model;

import lombok.Data;

/**
 * @author 超
 * Create by fengc on  2021/10/20 16:42
 * 数据库字段信息实体类
 */
@Data
public class ColumnModel {

    /**
     * 列名
     */
    private String columnName;
    /**
     * 列名类型
     */
    private String dataType;
    /**
     * 列表备注
     */
    private String comments;

    /**
     * 属性名称（第一个字母大写）user_name-> UserName
     */
    private String attrName;

    /**
     *属性名称（第一个字母小写） user_name -> userName
     */
    private String attrname;

    /**
     * 属性类型
     */
    private String attrType;

    /**
     *auto_increment
     */
    private String extra;

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    public String getAttrname() {
        return attrname;
    }

    public void setAttrname(String attrname) {
        this.attrname = attrname;
    }
}
