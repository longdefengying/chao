package com.chao.generator.service;

import com.chao.generator.dao.TableMapper;
import com.chao.generator.utils.GenerateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * @author 超
 * Create by fengc on  2021/10/21 19:15
 */
@Service
@Slf4j
public class TableService {

    @Resource
    private TableMapper tableMapper;

    public Map<String,String> findTableByTableName(String tableName) {

        return tableMapper.selectTable(tableName);
    }

    public List<Map<String,String>> listColumnByTableName(String tableName) {

        return tableMapper.selectColumns(tableName);
    }

    public byte[] generatorCode(List<String> tableNames,String moduleName) {
        ByteArrayOutputStream outPutStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outPutStream);
        tableNames.stream().forEach(tableName -> {
            Map<String,String> table = findTableByTableName(tableName);
            List<Map<String,String>> columns = listColumnByTableName(tableName);
            GenerateUtil.generator(moduleName,table,columns,zip);
            log.info("数据库表={}，代码生成......",table);
        });
        return outPutStream.toByteArray();
    }
}
