package com.chao.generator.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;
import com.central.common.constant.CommonConstant;
import com.chao.generator.model.ColumnModel;
import com.chao.generator.model.TableModel;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author 超
 * Create by fengc on  2021/6/21 14:31
 */
@Slf4j
public class GenerateUtil {

    private GenerateUtil() {

    }

    private final static String FILE_NAME_MODEL = "Model.java.vm";
    private final static String FILE_NAME_VO = "VO.java.vm";
    private final static String FILE_NAME_MAPPER = "Mapper.java.vm";
    private final static String FILE_NAME_MAPPER_XML = "Mapper.xml.vm";
    private final static String FILE_NAME_SERVICE = "Service.java.vm";
   // private final static String FILE_NAME_SERVICE_IMPL = "ServiceImpl.java.vm";
    private final static String FILE_NAME_CONTROLLER = "Controller.java.vm";
    private final static String FILE_PATH = "template/";
    private final static String PACKAGE = "package";
    private final static String MODULE_NAME = "moduleName";

    public static List<String> getTemplates() {

        return Lists.newArrayList(FILE_PATH + FILE_NAME_MODEL,FILE_PATH + FILE_NAME_VO,FILE_PATH + FILE_NAME_MAPPER,FILE_PATH + FILE_NAME_MAPPER_XML,
                FILE_PATH + FILE_NAME_SERVICE,
                //FILE_PATH + FILE_NAME_SERVICE_IMPL,
                FILE_PATH + FILE_NAME_CONTROLLER);
    }

    /**
     * 生成代码方法
     * @param moduleName 模块名
     * @param table
     * @param columns
     * @param zip
     */
    public static void generator(String moduleName,Map<String,String> table,List<Map<String,String>> columns, ZipOutputStream zip) {
        Configuration configuration = getGeneratorConfig();
        AtomicBoolean hasBigDecimal = new AtomicBoolean(false);

        //表信息
        TableModel tableModel = new TableModel();
        tableModel.setTableName(table.get("tableName"));
        tableModel.setComments(table.get("tableComment"));

        String className = tableToJava(tableModel.getTableName(),table.get("tablePrefix"));
        tableModel.setClassName(className);
        tableModel.setClassname(StringUtils.uncapitalize(className));

        //列信息
        List<ColumnModel> columnModels = Lists.newArrayList();
        columns.stream().forEach(columnMap -> {
            ColumnModel columnModel = new ColumnModel();
            columnModel.setColumnName(columnMap.get("columnName"));
            columnModel.setDataType(columnMap.get("dataType"));
            columnModel.setComments(columnMap.get("columnComment"));
            columnModel.setExtra(columnMap.get("extra"));

            //列名转为Java
            String attrName = columnToJava(columnModel.getColumnName());
            columnModel.setAttrName(attrName);
            columnModel.setAttrname(StringUtils.capitalize(attrName));
            //列的数据类型，转为Java类型
            String attrType = configuration.getString(columnModel.getDataType(),"unknowType");
            columnModel.setAttrType(attrType);
            if (!hasBigDecimal.get() && "BigDecimal".equals(attrType)) {
                hasBigDecimal.set(true);
            }
            //是否主键
            if ("PRI".equalsIgnoreCase(columnMap.get("columnKey")) && Objects.isNull(tableModel.getPk())) {
                tableModel.setPk(columnModel);
            }
            columnModels.add(columnModel);
        });
        tableModel.setColumns(columnModels);
        //没主键，设置第一个字段为主键
        if (Objects.isNull(tableModel.getPk())) {
            tableModel.setPk(columnModels.get(0));
        }
        //设置velocity
        Properties pro = new Properties();
        pro.put("file.resource.loader.class","org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        //定义字符集
        pro.setProperty(Velocity.ENCODING_DEFAULT, "UTF-8");
        pro.setProperty(Velocity.OUTPUT_ENCODING, "UTF-8");
        Velocity.init(pro);

        String mainPath = configuration.getString("mainPath");
        mainPath = StringUtils.isNotBlank(mainPath) ? mainPath : "io.ren";
        Map<String,Object> map = new HashMap<>();
        map.put("tableName",tableModel.getTableName());
        map.put("comments",tableModel.getComments());
        map.put("pk",tableModel.getPk());
        map.put("className",tableModel.getClassName());
        map.put("classname",tableModel.getClassname());
        map.put("pathName",tableModel.getClassname().toLowerCase());
        map.put("columns",tableModel.getColumns());
        map.put("hasBigDecimal",hasBigDecimal.get());
        map.put("mainPath",mainPath);
        map.put(PACKAGE,configuration.getString(PACKAGE));
        map.put("moduleName",moduleName);
        map.put("author",configuration.getString("author"));
        map.put("email",configuration.getString("email"));
        map.put("datetime", DateUtil.format(new Date(), CommonConstant.DATETIME_FORMAT));
        log.info("Velocity的参数设置={}",JSONUtil.toJsonPrettyStr(map));
        VelocityContext context = new VelocityContext(map);
        //获取模板列表
        List<String> templates = getTemplates();
        templates.forEach(template -> {
            try (StringWriter sw = new StringWriter()) {
                log.info("----当前的模板是：{}",template);
                Template tpl = Velocity.getTemplate(template,"UTF-8");
                tpl.merge(context,sw);
                //添加到zip
                String name = getFileName(template,tableModel.getClassName(),configuration.getString(PACKAGE),moduleName);
                log.info("----当前生成的文件名是：{}",name);
                zip.putNextEntry(new ZipEntry(name));
                IOUtils.write(sw.toString(),zip, StandardCharsets.UTF_8);
                zip.closeEntry();
            } catch (IOException ioException) {
                log.error("导出错误:",ioException);
            }
        });
    }

    /**
     * 文件名
     * @param templateName
     * @param className
     * @param packageName
     * @param moduleName
     * @return
     */
    public static String getFileName(String templateName,String className,String packageName,String moduleName) {
        String packagePath = "main" + File.separator + "java" +File.separator;
        if (StringUtils.isNotBlank(packageName)) {
            packagePath = packageName.replace(".",File.separator) + File.separator + moduleName + File.separator;
        }
        //model
        if (templateName.contains(FILE_NAME_MODEL)) {
            return packagePath + "model" + File.separator + className + ".java";
        }
        //VO
        if (templateName.contains(FILE_NAME_VO)) {
            return packagePath + "vo" + File.separator + className + "VO.java";
        }
        //dao
        if (templateName.contains(FILE_NAME_MAPPER)) {
            return packagePath + "mapper" + File.separator + className + "Mapper.java";
        }
        //Mapper xml
        if (templateName.contains(FILE_NAME_MAPPER_XML)) {
            return "main" + File.separator + "resources" + File.separator  + "mapper" + File.separator + className + "Mapper.xml";
        }
        //service
        if (templateName.contains(FILE_NAME_SERVICE)) {
            return packagePath + "service" + File.separator + className + "Service.java";
        }
        /*//serviceImpl
        if (templateName.contains(FILE_NAME_SERVICE_IMPL)) {
            return packagePath + "service" + File.separator + "impl" + File.separator + className + "ServiceImpl.java";
        }*/
        //controller
        if (templateName.contains(FILE_NAME_CONTROLLER)) {
            return packagePath + "controller" + File.separator + className + "Controller.java";
        }
        return "";
    }

    /**
     * 表名转为首字母大写的类名
     * @param tableName
     * @param tablePrefix
     * @return
     */
    public static String tableToJava(String tableName,String tablePrefix) {
        if (StringUtils.isNotBlank(tablePrefix)) {
            tableName = tableName.substring(tablePrefix.length());
        }
        return columnToJava(tableName);
    }

    /**
     * 字段名转为首字母也大写的字段
     * @param columnName
     * @return
     */
    public static String columnToJava(String columnName) {
        return WordUtils.capitalizeFully(columnName,new char[]{'_'}).replace("_","");
    }

    public static Configuration getGeneratorConfig() {
        try {
            return new PropertiesConfiguration("generator.properties");
        } catch (Exception e) {
            throw new RuntimeException("读取生成代码的配置文件失败!");
        }
    }

    public static void main(String[] args) {
        String columnName = "user_name";
        System.out.println(WordUtils.capitalizeFully(columnName,new char[]{'_'}));
        System.out.println(columnToJava(columnName));
    }

}
