package com.chao.generator.config;

import com.chao.generator.config.po.TableField;
import com.chao.generator.config.rules.IColumnType;

/**
 * @author 超
 * Create by fengc on  2021/7/12 23:55
 * 数据库子弹类型转换
 */
public interface ITypeConvert {

    default IColumnType processTypeConvert(GlobalConfig globalConfig, TableField tableField) {
        return processTypeCovert(globalConfig,tableField.getType());
    }

    /**
     * 执行类型转换
     * @param globalConfig 全局配置
     * @param fieldType 字段类型
     * @return
     */
    IColumnType processTypeCovert(GlobalConfig globalConfig,String fieldType);
}
