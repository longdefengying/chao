package com.chao.user.controller;

import com.central.common.model.Result;
import com.chao.user.model.SysUser;
import com.chao.user.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 
 *
 * @author chao
 * @date 2021-11-16 16:08:24
 */
@Slf4j
@RestController
@RequestMapping("/sysuser")
@Api(tags = "")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    /**
     * 列表
     */
   /* @ApiOperation(value = "查询列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "分页起始位置", required = true, dataType = "Integer"),
            @ApiImplicitParam(name = "limit", value = "分页结束位置", required = true, dataType = "Integer")
    })
    @GetMapping
    public PageResult list(@RequestParam Map<String, Object> params) {
        return sysUserService.findList(params);
    }*/

    /**
     * 查询
     */
    @ApiOperation(value = "查询")
    @GetMapping("/{Id}")
    public Result findUserById(@PathVariable Long Id) {
        SysUser model = sysUserService.getById(Id);
        return Result.success(model, "查询成功");
    }

    /**
     * 新增or更新
     */
    @ApiOperation(value = "保存")
    @PostMapping
    public Result save(@RequestBody SysUser sysUser) {
        sysUserService.saveOrUpdate(sysUser);
        return Result.success("保存成功");
    }

    /**
     * 删除
     */
    @ApiOperation(value = "删除")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Long Id) {
        sysUserService.removeById(Id);
        return Result.success("删除成功");
    }
}
