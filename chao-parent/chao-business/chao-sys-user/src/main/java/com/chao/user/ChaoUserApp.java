package com.chao.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 超
 * Create by fengc on  2021/10/19 16:20
 */
@SpringBootApplication
public class ChaoUserApp {

    public static void main(String[] args) {

        //BasicConfigurator.configure();
        SpringApplication.run(ChaoUserApp.class, args);
        System.out.println("项目启动成功....................................");
    }

}
