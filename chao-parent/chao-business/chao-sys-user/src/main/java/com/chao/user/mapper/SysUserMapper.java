package com.chao.user.mapper;

import com.chao.user.model.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author chao
 * @date 2021-11-16 16:08:24
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

}
