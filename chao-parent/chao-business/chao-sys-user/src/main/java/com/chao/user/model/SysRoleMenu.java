package com.chao.user.model;

import com.central.common.model.SuperEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
 * 
 *
 * @author chao
 * @date 2021-11-16 16:08:24
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@TableName("sys_role_menu")
public class SysRoleMenu extends SuperEntity {
    private static final long serialVersionUID=1L;

        private Integer MenuId;
    }
